# statemachine-EclipsePlugIn

Dieses Repository bietet eine Beispiel Implementierung einer model-getriebenen Softwareentwicklungs-Pipeline (MDSE/MDSD).

Die verwendete Technologie ist EMF mit den Plug-Ins Xtext, Epsilon, und Sirius.
Die verwendete Entwicklungsumgebung ist Eclipse mit dem *Eclipse Modeling Tools*.
Die ausgegeben Artefakte sind Eclipse Plug-Ins.


## Vorbereitung

1. Installieren Sie die [*Eclipse Modeling Tools*](https://www.eclipse.org/downloads/packages/release/2024-06/r/eclipse-modeling-tools). Die EMT sind eine vorbereitete Eclipse Instanz mit allen grundlegenden Plugins und ist als standalone Version für alle Betriebssysteme verfügbar.
2. Installieren Sie die folgenden beiden Plugins in der entpacken, gestarteten Eclipse Instanz mittels `Help` → `Eclipse Marketplace`.
    - Eclipse Xtext
    - Epsilon
    - Sirius
    Alle Plugins können in der aktuellsten Version im kompletten Umfang installiert werden.
3. Unter folgenden Links können Sie die Dokumentationen finden, welche Sie zum Workshop bereit halten sollten
    - [Xtext](https://eclipse.dev/Xtext/documentation/301_grammarlanguage.html)
    - [Epsilon (EOL, EVL, EXL/EGL)](https://eclipse.dev/epsilon/doc/eol/)
    - [Sirius](https://wiki.eclipse.org/Sirius/Tutorials/StarterTutorial)

## Installation

Alle Projekte können in der IDE mittels `Import...` → `Projects from Folder ...` in den Model Explorer (links) geladen werden.

Importieren Sie in den direkten Workspace:

- Das State Machine Metamodell: 
    - `./metamodel/swt.most.statemachine`
    - `./metamodel/swt.most.statemachine.edit`
    - `./metamodel/swt.most.statemachine.editor`

- Das PetriNet Metamodell (wenn Model-to-Model Transformation verwendet werden soll):
    - `./metamodel/swt.most.petrinet`
    - `./metamodel/swt.most.petrinet.edit`
    - `./metamodel/swt.most.petrinet.editor`

- Die textuelle DSL, wenn diese für die State Machine verwendet werden soll
    - `./domain_specific_language/textual/swt.most.statemachine.xtext`

Mittels Rechtsklick auf eines der Projekte und `Run As...` → `Eclipse Application` wird ein Runtime-Workspace gestartet.

Importieren Sie in den direkten Runtime-Workspace:

- Die Graphische DSL der State Machine (wenn keine textuelle DSL für die State Machine verwendet wird):
    - `./domain_specific_language/graphical/swt.most.statemachine.sirius`
- Die Graphische DSL des PetriNets (wenn das PetriNet Metamodell verwendet wird):
    - `./domain_specific_language/graphical/swt.most.statemachine.sirius`

- Der Transformator der State Machine (benötigt beide Metamodelle)
    - `./model2model/swt.most.statemachine.to.petrinet.etl`

- Der Validator der State Machine
    - `./model2model/swt.most.statemachine.validation.evl`

- Der Java Code Generator (Model-to-Text) der StateMachine
    - `./model2text/swt.most.statemachine.to.java.egl`

## Beispiele

Alle Teile der MDSD-Pipeline können einzeln oder in Reihe verwendet werden. Mur sollte sich auf eine DSL Art (graphisch oder textuell) geeinigt werden.

Für jeden Teil liegt ein Beispielprojekt bei. Normalerweise liegen die Beispiele im XMI Format bereit. Sollte die textuelle DSL Verwendet werden, können die textuellen Modelle automatisch mit den vorliegenden Transformatoren, Validatoren und Model-to-Text-Generatoren verwendet werden, solange die DSL im direkten Workspace geladen ist. Bitte verwenden Sie dann die Beispielmodelle der textuellen DSL.

## Verwendung

- Für Epsilon Applikationen liegen `.launch` Konfigurationen bei die mit `Run As ...` gestartet werden können. (Wenn eine textuelle DSL verwendet wir , müssen die XMI-basierten Modelle zuvor durch textuelle Modelle ausgetauscht werden)
    - Ansonsten kann über `Run As ...` eine neue Konfiguration angelegt werden. Hier die jeweilige script Datei als `source` auswählen und unter `models` mittels `Add...` ein EMF Modell mit Namen `source` und beim Transformator ein leeres EMF Modell mit Namen `target` hinzufügen. (für beide muss über `Model File` eine Datei ausgewählt werden.)
- Bei Sirius basierten DSLs können die Editoren durch die `.aird` Datei geöffnet werden (Datei im Explorer aufklappen) oder durch Rechtsklick auf das Projekt mittels `create Representation` erstellt werden.

Für weitere Hinweise zum Ausführen schauen Sie sich bitte die jeweilige Dokumentation des Plugins an.