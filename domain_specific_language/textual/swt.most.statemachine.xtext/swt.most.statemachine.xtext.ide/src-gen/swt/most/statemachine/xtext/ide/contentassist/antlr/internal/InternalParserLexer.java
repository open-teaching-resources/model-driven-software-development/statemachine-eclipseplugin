package swt.most.statemachine.xtext.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalParserLexer extends Lexer {
    public static final int RULE_STRING=4;
    public static final int RULE_INITIALSTATENAME=7;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_CALL=6;
    public static final int RULE_ID=5;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_FINALSTATENAME=8;
    public static final int RULE_INT=9;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalParserLexer() {;} 
    public InternalParserLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalParserLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalParser.g"; }

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:11:7: ( 'StateMachine' )
            // InternalParser.g:11:9: 'StateMachine'
            {
            match("StateMachine"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:12:7: ( '{' )
            // InternalParser.g:12:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:13:7: ( '}' )
            // InternalParser.g:13:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:14:7: ( 'state' )
            // InternalParser.g:14:9: 'state'
            {
            match("state"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:15:7: ( 'entry:' )
            // InternalParser.g:15:9: 'entry:'
            {
            match("entry:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:16:7: ( 'do:' )
            // InternalParser.g:16:9: 'do:'
            {
            match("do:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:17:7: ( 'exit:' )
            // InternalParser.g:17:9: 'exit:'
            {
            match("exit:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:18:7: ( 'end' )
            // InternalParser.g:18:9: 'end'
            {
            match("end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:19:7: ( 'start' )
            // InternalParser.g:19:9: 'start'
            {
            match("start"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:20:7: ( '-' )
            // InternalParser.g:20:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:21:7: ( '->' )
            // InternalParser.g:21:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:22:7: ( '[' )
            // InternalParser.g:22:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:23:7: ( ']' )
            // InternalParser.g:23:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:24:7: ( '/' )
            // InternalParser.g:24:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "RULE_INITIALSTATENAME"
    public final void mRULE_INITIALSTATENAME() throws RecognitionException {
        try {
            int _type = RULE_INITIALSTATENAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1951:23: ( 'I_' ( RULE_INT | RULE_STRING | RULE_ID )+ )
            // InternalParser.g:1951:25: 'I_' ( RULE_INT | RULE_STRING | RULE_ID )+
            {
            match("I_"); 

            // InternalParser.g:1951:30: ( RULE_INT | RULE_STRING | RULE_ID )+
            int cnt1=0;
            loop1:
            do {
                int alt1=4;
                switch ( input.LA(1) ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt1=1;
                    }
                    break;
                case '\"':
                case '\'':
                    {
                    alt1=2;
                    }
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '^':
                case '_':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt1=3;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // InternalParser.g:1951:31: RULE_INT
            	    {
            	    mRULE_INT(); 

            	    }
            	    break;
            	case 2 :
            	    // InternalParser.g:1951:40: RULE_STRING
            	    {
            	    mRULE_STRING(); 

            	    }
            	    break;
            	case 3 :
            	    // InternalParser.g:1951:52: RULE_ID
            	    {
            	    mRULE_ID(); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INITIALSTATENAME"

    // $ANTLR start "RULE_FINALSTATENAME"
    public final void mRULE_FINALSTATENAME() throws RecognitionException {
        try {
            int _type = RULE_FINALSTATENAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1953:21: ( 'F_' ( RULE_INT | RULE_STRING | RULE_ID )+ )
            // InternalParser.g:1953:23: 'F_' ( RULE_INT | RULE_STRING | RULE_ID )+
            {
            match("F_"); 

            // InternalParser.g:1953:28: ( RULE_INT | RULE_STRING | RULE_ID )+
            int cnt2=0;
            loop2:
            do {
                int alt2=4;
                switch ( input.LA(1) ) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    {
                    alt2=1;
                    }
                    break;
                case '\"':
                case '\'':
                    {
                    alt2=2;
                    }
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '^':
                case '_':
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                    {
                    alt2=3;
                    }
                    break;

                }

                switch (alt2) {
            	case 1 :
            	    // InternalParser.g:1953:29: RULE_INT
            	    {
            	    mRULE_INT(); 

            	    }
            	    break;
            	case 2 :
            	    // InternalParser.g:1953:38: RULE_STRING
            	    {
            	    mRULE_STRING(); 

            	    }
            	    break;
            	case 3 :
            	    // InternalParser.g:1953:50: RULE_ID
            	    {
            	    mRULE_ID(); 

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FINALSTATENAME"

    // $ANTLR start "RULE_CALL"
    public final void mRULE_CALL() throws RecognitionException {
        try {
            int _type = RULE_CALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1955:11: ( RULE_ID ( '.' RULE_ID )+ )
            // InternalParser.g:1955:13: RULE_ID ( '.' RULE_ID )+
            {
            mRULE_ID(); 
            // InternalParser.g:1955:21: ( '.' RULE_ID )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='.') ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalParser.g:1955:22: '.' RULE_ID
            	    {
            	    match('.'); 
            	    mRULE_ID(); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_CALL"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1957:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalParser.g:1957:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalParser.g:1957:11: ( '^' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='^') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalParser.g:1957:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalParser.g:1957:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')||(LA5_0>='A' && LA5_0<='Z')||LA5_0=='_'||(LA5_0>='a' && LA5_0<='z')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalParser.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            // InternalParser.g:1959:19: ( ( '0' .. '9' )+ )
            // InternalParser.g:1959:21: ( '0' .. '9' )+
            {
            // InternalParser.g:1959:21: ( '0' .. '9' )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalParser.g:1959:22: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1961:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalParser.g:1961:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalParser.g:1961:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='\"') ) {
                alt9=1;
            }
            else if ( (LA9_0=='\'') ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalParser.g:1961:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalParser.g:1961:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0=='\\') ) {
                            alt7=1;
                        }
                        else if ( ((LA7_0>='\u0000' && LA7_0<='!')||(LA7_0>='#' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='\uFFFF')) ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalParser.g:1961:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalParser.g:1961:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalParser.g:1961:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalParser.g:1961:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='&')||(LA8_0>='(' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalParser.g:1961:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalParser.g:1961:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1963:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalParser.g:1963:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalParser.g:1963:24: ( options {greedy=false; } : . )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0=='*') ) {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1=='/') ) {
                        alt10=2;
                    }
                    else if ( ((LA10_1>='\u0000' && LA10_1<='.')||(LA10_1>='0' && LA10_1<='\uFFFF')) ) {
                        alt10=1;
                    }


                }
                else if ( ((LA10_0>='\u0000' && LA10_0<=')')||(LA10_0>='+' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalParser.g:1963:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1965:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalParser.g:1965:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalParser.g:1965:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\u0000' && LA11_0<='\t')||(LA11_0>='\u000B' && LA11_0<='\f')||(LA11_0>='\u000E' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalParser.g:1965:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // InternalParser.g:1965:40: ( ( '\\r' )? '\\n' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='\n'||LA13_0=='\r') ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalParser.g:1965:41: ( '\\r' )? '\\n'
                    {
                    // InternalParser.g:1965:41: ( '\\r' )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0=='\r') ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // InternalParser.g:1965:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1967:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalParser.g:1967:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalParser.g:1967:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt14=0;
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>='\t' && LA14_0<='\n')||LA14_0=='\r'||LA14_0==' ') ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalParser.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalParser.g:1969:16: ( . )
            // InternalParser.g:1969:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalParser.g:1:8: ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | RULE_INITIALSTATENAME | RULE_FINALSTATENAME | RULE_CALL | RULE_ID | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt15=23;
        alt15 = dfa15.predict(input);
        switch (alt15) {
            case 1 :
                // InternalParser.g:1:10: T__14
                {
                mT__14(); 

                }
                break;
            case 2 :
                // InternalParser.g:1:16: T__15
                {
                mT__15(); 

                }
                break;
            case 3 :
                // InternalParser.g:1:22: T__16
                {
                mT__16(); 

                }
                break;
            case 4 :
                // InternalParser.g:1:28: T__17
                {
                mT__17(); 

                }
                break;
            case 5 :
                // InternalParser.g:1:34: T__18
                {
                mT__18(); 

                }
                break;
            case 6 :
                // InternalParser.g:1:40: T__19
                {
                mT__19(); 

                }
                break;
            case 7 :
                // InternalParser.g:1:46: T__20
                {
                mT__20(); 

                }
                break;
            case 8 :
                // InternalParser.g:1:52: T__21
                {
                mT__21(); 

                }
                break;
            case 9 :
                // InternalParser.g:1:58: T__22
                {
                mT__22(); 

                }
                break;
            case 10 :
                // InternalParser.g:1:64: T__23
                {
                mT__23(); 

                }
                break;
            case 11 :
                // InternalParser.g:1:70: T__24
                {
                mT__24(); 

                }
                break;
            case 12 :
                // InternalParser.g:1:76: T__25
                {
                mT__25(); 

                }
                break;
            case 13 :
                // InternalParser.g:1:82: T__26
                {
                mT__26(); 

                }
                break;
            case 14 :
                // InternalParser.g:1:88: T__27
                {
                mT__27(); 

                }
                break;
            case 15 :
                // InternalParser.g:1:94: RULE_INITIALSTATENAME
                {
                mRULE_INITIALSTATENAME(); 

                }
                break;
            case 16 :
                // InternalParser.g:1:116: RULE_FINALSTATENAME
                {
                mRULE_FINALSTATENAME(); 

                }
                break;
            case 17 :
                // InternalParser.g:1:136: RULE_CALL
                {
                mRULE_CALL(); 

                }
                break;
            case 18 :
                // InternalParser.g:1:146: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 19 :
                // InternalParser.g:1:154: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 20 :
                // InternalParser.g:1:166: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 21 :
                // InternalParser.g:1:182: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 22 :
                // InternalParser.g:1:198: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 23 :
                // InternalParser.g:1:206: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA15 dfa15 = new DFA15(this);
    static final String DFA15_eotS =
        "\1\uffff\1\25\2\uffff\3\25\1\36\2\uffff\1\43\2\25\1\22\1\25\2\22\2\uffff\2\25\4\uffff\4\25\7\uffff\3\25\2\uffff\3\25\1\71\1\25\1\uffff\2\61\1\uffff\1\63\1\uffff\1\63\4\25\1\uffff\1\25\2\61\2\63\1\25\1\105\1\106\1\25\1\uffff\1\25\3\uffff\5\25\1\116\1\uffff";
    static final String DFA15_eofS =
        "\117\uffff";
    static final String DFA15_minS =
        "\1\0\1\56\2\uffff\3\56\1\76\2\uffff\1\52\2\56\1\101\1\56\2\0\2\uffff\2\56\4\uffff\4\56\7\uffff\2\42\1\56\2\uffff\5\56\1\uffff\2\56\1\uffff\1\56\1\uffff\5\56\1\uffff\11\56\1\uffff\1\56\3\uffff\6\56\1\uffff";
    static final String DFA15_maxS =
        "\1\uffff\1\172\2\uffff\3\172\1\76\2\uffff\1\57\4\172\2\uffff\2\uffff\2\172\4\uffff\4\172\7\uffff\3\172\2\uffff\5\172\1\uffff\2\172\1\uffff\1\172\1\uffff\5\172\1\uffff\11\172\1\uffff\1\172\3\uffff\6\172\1\uffff";
    static final String DFA15_acceptS =
        "\2\uffff\1\2\1\3\4\uffff\1\14\1\15\7\uffff\1\26\1\27\2\uffff\1\22\1\21\1\2\1\3\4\uffff\1\13\1\12\1\14\1\15\1\24\1\25\1\16\3\uffff\1\23\1\26\5\uffff\1\6\2\uffff\1\17\1\uffff\1\20\5\uffff\1\10\11\uffff\1\7\1\uffff\1\4\1\11\1\5\6\uffff\1\1";
    static final String DFA15_specialS =
        "\1\0\16\uffff\1\1\1\2\76\uffff}>";
    static final String[] DFA15_transitionS = {
            "\11\22\2\21\2\22\1\21\22\22\1\21\1\22\1\17\4\22\1\20\5\22\1\7\1\22\1\12\21\22\5\16\1\14\2\16\1\13\11\16\1\1\7\16\1\10\1\22\1\11\1\15\1\16\1\22\3\16\1\6\1\5\15\16\1\4\7\16\1\2\1\22\1\3\uff82\22",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\23\24\1\23\6\24",
            "",
            "",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\23\24\1\31\6\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\15\24\1\32\11\24\1\33\2\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\16\24\1\34\13\24",
            "\1\35",
            "",
            "",
            "\1\41\4\uffff\1\42",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\44\1\uffff\32\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\45\1\uffff\32\24",
            "\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "\0\47",
            "\0\47",
            "",
            "",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\1\51\31\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "",
            "",
            "",
            "",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\1\52\31\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\3\24\1\54\17\24\1\53\6\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\10\24\1\55\21\24",
            "\1\26\1\uffff\12\24\1\56\6\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\61\4\uffff\1\61\6\uffff\1\26\1\uffff\12\57\7\uffff\32\60\3\uffff\1\61\1\60\1\uffff\32\60",
            "\1\63\4\uffff\1\63\6\uffff\1\26\1\uffff\12\62\7\uffff\32\64\3\uffff\1\63\1\64\1\uffff\32\64",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "",
            "",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\23\24\1\65\6\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\21\24\1\67\1\24\1\66\6\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\21\24\1\70\10\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\23\24\1\72\6\24",
            "",
            "\1\26\1\uffff\12\57\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\26\1\uffff\12\73\7\uffff\32\74\4\uffff\1\74\1\uffff\32\74",
            "",
            "\1\26\1\uffff\12\62\7\uffff\32\64\4\uffff\1\64\1\uffff\32\64",
            "",
            "\1\26\1\uffff\12\75\7\uffff\32\76\4\uffff\1\76\1\uffff\32\76",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\4\24\1\77\25\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\4\24\1\100\25\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\23\24\1\101\6\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\30\24\1\102\1\24",
            "",
            "\1\26\1\uffff\12\24\1\103\6\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "\1\26\1\uffff\12\73\7\uffff\32\74\4\uffff\1\74\1\uffff\32\74",
            "\1\26\1\uffff\12\73\7\uffff\32\74\4\uffff\1\74\1\uffff\32\74",
            "\1\26\1\uffff\12\75\7\uffff\32\76\4\uffff\1\76\1\uffff\32\76",
            "\1\26\1\uffff\12\75\7\uffff\32\76\4\uffff\1\76\1\uffff\32\76",
            "\1\26\1\uffff\12\24\7\uffff\14\24\1\104\15\24\4\uffff\1\24\1\uffff\32\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "\1\26\1\uffff\12\24\1\107\6\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            "",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\1\110\31\24",
            "",
            "",
            "",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\2\24\1\111\27\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\7\24\1\112\22\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\10\24\1\113\21\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\15\24\1\114\14\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\4\24\1\115\25\24",
            "\1\26\1\uffff\12\24\7\uffff\32\24\4\uffff\1\24\1\uffff\32\24",
            ""
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | RULE_INITIALSTATENAME | RULE_FINALSTATENAME | RULE_CALL | RULE_ID | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA15_0 = input.LA(1);

                        s = -1;
                        if ( (LA15_0=='S') ) {s = 1;}

                        else if ( (LA15_0=='{') ) {s = 2;}

                        else if ( (LA15_0=='}') ) {s = 3;}

                        else if ( (LA15_0=='s') ) {s = 4;}

                        else if ( (LA15_0=='e') ) {s = 5;}

                        else if ( (LA15_0=='d') ) {s = 6;}

                        else if ( (LA15_0=='-') ) {s = 7;}

                        else if ( (LA15_0=='[') ) {s = 8;}

                        else if ( (LA15_0==']') ) {s = 9;}

                        else if ( (LA15_0=='/') ) {s = 10;}

                        else if ( (LA15_0=='I') ) {s = 11;}

                        else if ( (LA15_0=='F') ) {s = 12;}

                        else if ( (LA15_0=='^') ) {s = 13;}

                        else if ( ((LA15_0>='A' && LA15_0<='E')||(LA15_0>='G' && LA15_0<='H')||(LA15_0>='J' && LA15_0<='R')||(LA15_0>='T' && LA15_0<='Z')||LA15_0=='_'||(LA15_0>='a' && LA15_0<='c')||(LA15_0>='f' && LA15_0<='r')||(LA15_0>='t' && LA15_0<='z')) ) {s = 14;}

                        else if ( (LA15_0=='\"') ) {s = 15;}

                        else if ( (LA15_0=='\'') ) {s = 16;}

                        else if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {s = 17;}

                        else if ( ((LA15_0>='\u0000' && LA15_0<='\b')||(LA15_0>='\u000B' && LA15_0<='\f')||(LA15_0>='\u000E' && LA15_0<='\u001F')||LA15_0=='!'||(LA15_0>='#' && LA15_0<='&')||(LA15_0>='(' && LA15_0<=',')||LA15_0=='.'||(LA15_0>='0' && LA15_0<='@')||LA15_0=='\\'||LA15_0=='`'||LA15_0=='|'||(LA15_0>='~' && LA15_0<='\uFFFF')) ) {s = 18;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA15_15 = input.LA(1);

                        s = -1;
                        if ( ((LA15_15>='\u0000' && LA15_15<='\uFFFF')) ) {s = 39;}

                        else s = 18;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA15_16 = input.LA(1);

                        s = -1;
                        if ( ((LA15_16>='\u0000' && LA15_16<='\uFFFF')) ) {s = 39;}

                        else s = 18;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 15, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}