/*
 * generated by Xtext 2.35.0
 */
grammar InternalParser;

options {
	superClass=AbstractInternalAntlrParser;
}

@lexer::header {
package swt.most.statemachine.xtext.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package swt.most.statemachine.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import swt.most.statemachine.xtext.services.ParserGrammarAccess;

}

@parser::members {

 	private ParserGrammarAccess grammarAccess;

    public InternalParserParser(TokenStream input, ParserGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }

    @Override
    protected String getFirstRuleName() {
    	return "StateMachine";
   	}

   	@Override
   	protected ParserGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}

}

@rulecatch {
    catch (RecognitionException re) {
        recover(input,re);
        appendSkippedTokens();
    }
}

// Entry rule entryRuleStateMachine
entryRuleStateMachine returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getStateMachineRule()); }
	iv_ruleStateMachine=ruleStateMachine
	{ $current=$iv_ruleStateMachine.current; }
	EOF;

// Rule StateMachine
ruleStateMachine returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getStateMachineAccess().getStateMachineAction_0(),
					$current);
			}
		)
		otherlv_1='StateMachine'
		{
			newLeafNode(otherlv_1, grammarAccess.getStateMachineAccess().getStateMachineKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getStateMachineAccess().getNameEStringParserRuleCall_2_0());
				}
				lv_name_2_0=ruleEString
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getStateMachineRule());
					}
					set(
						$current,
						"name",
						lv_name_2_0,
						"swt.most.statemachine.xtext.Parser.EString");
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_3='{'
		{
			newLeafNode(otherlv_3, grammarAccess.getStateMachineAccess().getLeftCurlyBracketKeyword_3());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getStateMachineAccess().getInitialstateInitialStateParserRuleCall_4_0());
				}
				lv_initialstate_4_0=ruleInitialState
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getStateMachineRule());
					}
					set(
						$current,
						"initialstate",
						lv_initialstate_4_0,
						"swt.most.statemachine.xtext.Parser.InitialState");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			(
				(
					{
						newCompositeNode(grammarAccess.getStateMachineAccess().getStatesNormalStateParserRuleCall_5_0_0());
					}
					lv_states_5_0=ruleNormalState
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getStateMachineRule());
						}
						add(
							$current,
							"states",
							lv_states_5_0,
							"swt.most.statemachine.xtext.Parser.NormalState");
						afterParserOrEnumRuleCall();
					}
				)
			)
			    |
			(
				(
					{
						newCompositeNode(grammarAccess.getStateMachineAccess().getFinalstatesFinalStateParserRuleCall_5_1_0());
					}
					lv_finalstates_6_0=ruleFinalState
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getStateMachineRule());
						}
						add(
							$current,
							"finalstates",
							lv_finalstates_6_0,
							"swt.most.statemachine.xtext.Parser.FinalState");
						afterParserOrEnumRuleCall();
					}
				)
			)
			    |
			(
				(
					{
						newCompositeNode(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_5_2_0());
					}
					lv_transitions_7_0=ruleTransition
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getStateMachineRule());
						}
						add(
							$current,
							"transitions",
							lv_transitions_7_0,
							"swt.most.statemachine.xtext.Parser.Transition");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)*
		otherlv_8='}'
		{
			newLeafNode(otherlv_8, grammarAccess.getStateMachineAccess().getRightCurlyBracketKeyword_6());
		}
	)
;

// Entry rule entryRuleNormalState
entryRuleNormalState returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getNormalStateRule()); }
	iv_ruleNormalState=ruleNormalState
	{ $current=$iv_ruleNormalState.current; }
	EOF;

// Rule NormalState
ruleNormalState returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getNormalStateAccess().getNormalStateAction_0(),
					$current);
			}
		)
		otherlv_1='state'
		{
			newLeafNode(otherlv_1, grammarAccess.getNormalStateAccess().getStateKeyword_1());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getNormalStateAccess().getNameEStringParserRuleCall_2_0());
				}
				lv_name_2_0=ruleEString
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getNormalStateRule());
					}
					set(
						$current,
						"name",
						lv_name_2_0,
						"swt.most.statemachine.xtext.Parser.EString");
					afterParserOrEnumRuleCall();
				}
			)
		)
		(
			otherlv_3='{'
			{
				newLeafNode(otherlv_3, grammarAccess.getNormalStateAccess().getLeftCurlyBracketKeyword_3_0());
			}
			(
				otherlv_4='entry:'
				{
					newLeafNode(otherlv_4, grammarAccess.getNormalStateAccess().getEntryKeyword_3_1_0());
				}
				(
					(
						{
							newCompositeNode(grammarAccess.getNormalStateAccess().getEntry_ActionParserRuleCall_3_1_1_0());
						}
						lv_entry__5_0=ruleAction
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getNormalStateRule());
							}
							set(
								$current,
								"entry_",
								lv_entry__5_0,
								"swt.most.statemachine.xtext.Parser.Action");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)?
			(
				otherlv_6='do:'
				{
					newLeafNode(otherlv_6, grammarAccess.getNormalStateAccess().getDoKeyword_3_2_0());
				}
				(
					(
						{
							newCompositeNode(grammarAccess.getNormalStateAccess().getDo_ActionParserRuleCall_3_2_1_0());
						}
						lv_do__7_0=ruleAction
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getNormalStateRule());
							}
							set(
								$current,
								"do_",
								lv_do__7_0,
								"swt.most.statemachine.xtext.Parser.Action");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)?
			(
				otherlv_8='exit:'
				{
					newLeafNode(otherlv_8, grammarAccess.getNormalStateAccess().getExitKeyword_3_3_0());
				}
				(
					(
						{
							newCompositeNode(grammarAccess.getNormalStateAccess().getExit_ActionParserRuleCall_3_3_1_0());
						}
						lv_exit__9_0=ruleAction
						{
							if ($current==null) {
								$current = createModelElementForParent(grammarAccess.getNormalStateRule());
							}
							set(
								$current,
								"exit_",
								lv_exit__9_0,
								"swt.most.statemachine.xtext.Parser.Action");
							afterParserOrEnumRuleCall();
						}
					)
				)
			)?
			otherlv_10='}'
			{
				newLeafNode(otherlv_10, grammarAccess.getNormalStateAccess().getRightCurlyBracketKeyword_3_4());
			}
		)?
	)
;

// Entry rule entryRuleFinalState
entryRuleFinalState returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getFinalStateRule()); }
	iv_ruleFinalState=ruleFinalState
	{ $current=$iv_ruleFinalState.current; }
	EOF;

// Rule FinalState
ruleFinalState returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getFinalStateAccess().getFinalStateAction_0(),
					$current);
			}
		)
		otherlv_1='end'
		{
			newLeafNode(otherlv_1, grammarAccess.getFinalStateAccess().getEndKeyword_1());
		}
		(
			(
				lv_name_2_0=RULE_FINALSTATENAME
				{
					newLeafNode(lv_name_2_0, grammarAccess.getFinalStateAccess().getNameFINALSTATENAMETerminalRuleCall_2_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getFinalStateRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_2_0,
						"swt.most.statemachine.xtext.Parser.FINALSTATENAME");
				}
			)
		)
	)
;

// Entry rule entryRuleInitialState
entryRuleInitialState returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getInitialStateRule()); }
	iv_ruleInitialState=ruleInitialState
	{ $current=$iv_ruleInitialState.current; }
	EOF;

// Rule InitialState
ruleInitialState returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getInitialStateAccess().getInitialStateAction_0(),
					$current);
			}
		)
		otherlv_1='start'
		{
			newLeafNode(otherlv_1, grammarAccess.getInitialStateAccess().getStartKeyword_1());
		}
		(
			(
				lv_name_2_0=RULE_INITIALSTATENAME
				{
					newLeafNode(lv_name_2_0, grammarAccess.getInitialStateAccess().getNameINITIALSTATENAMETerminalRuleCall_2_0());
				}
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getInitialStateRule());
					}
					setWithLastConsumed(
						$current,
						"name",
						lv_name_2_0,
						"swt.most.statemachine.xtext.Parser.INITIALSTATENAME");
				}
			)
		)
	)
;

// Entry rule entryRuleTransition
entryRuleTransition returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getTransitionRule()); }
	iv_ruleTransition=ruleTransition
	{ $current=$iv_ruleTransition.current; }
	EOF;

// Rule Transition
ruleTransition returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getTransitionAccess().getTransitionAction_0(),
					$current);
			}
		)
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getTransitionRule());
					}
				}
				{
					newCompositeNode(grammarAccess.getTransitionAccess().getFromStateCrossReference_1_0());
				}
				ruleEString
				{
					afterParserOrEnumRuleCall();
				}
			)
		)
		otherlv_2='-'
		{
			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getHyphenMinusKeyword_2());
		}
		(
			(
				{
					newCompositeNode(grammarAccess.getTransitionAccess().getTriggerTriggerParserRuleCall_3_0());
				}
				lv_trigger_3_0=ruleTrigger
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getTransitionRule());
					}
					set(
						$current,
						"trigger",
						lv_trigger_3_0,
						"swt.most.statemachine.xtext.Parser.Trigger");
					afterParserOrEnumRuleCall();
				}
			)
		)?
		(
			otherlv_4='['
			{
				newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_4_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getTransitionAccess().getGuardGuardParserRuleCall_4_1_0());
					}
					lv_guard_5_0=ruleGuard
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getTransitionRule());
						}
						set(
							$current,
							"guard",
							lv_guard_5_0,
							"swt.most.statemachine.xtext.Parser.Guard");
						afterParserOrEnumRuleCall();
					}
				)
			)
			otherlv_6=']'
			{
				newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getRightSquareBracketKeyword_4_2());
			}
		)?
		(
			otherlv_7='/'
			{
				newLeafNode(otherlv_7, grammarAccess.getTransitionAccess().getSolidusKeyword_5_0());
			}
			(
				(
					{
						newCompositeNode(grammarAccess.getTransitionAccess().getActionActionParserRuleCall_5_1_0());
					}
					lv_action_8_0=ruleAction
					{
						if ($current==null) {
							$current = createModelElementForParent(grammarAccess.getTransitionRule());
						}
						set(
							$current,
							"action",
							lv_action_8_0,
							"swt.most.statemachine.xtext.Parser.Action");
						afterParserOrEnumRuleCall();
					}
				)
			)
		)?
		otherlv_9='->'
		{
			newLeafNode(otherlv_9, grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_6());
		}
		(
			(
				{
					if ($current==null) {
						$current = createModelElement(grammarAccess.getTransitionRule());
					}
				}
				{
					newCompositeNode(grammarAccess.getTransitionAccess().getToStateCrossReference_7_0());
				}
				ruleEString
				{
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleAction
entryRuleAction returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getActionRule()); }
	iv_ruleAction=ruleAction
	{ $current=$iv_ruleAction.current; }
	EOF;

// Rule Action
ruleAction returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getActionAccess().getActionAction_0(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getActionAccess().getContentActivityContentParserRuleCall_1_0());
				}
				lv_content_1_0=ruleActivityContent
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getActionRule());
					}
					set(
						$current,
						"content",
						lv_content_1_0,
						"swt.most.statemachine.xtext.Parser.ActivityContent");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleTrigger
entryRuleTrigger returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getTriggerRule()); }
	iv_ruleTrigger=ruleTrigger
	{ $current=$iv_ruleTrigger.current; }
	EOF;

// Rule Trigger
ruleTrigger returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getTriggerAccess().getTriggerAction_0(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getTriggerAccess().getContentActivityContentParserRuleCall_1_0());
				}
				lv_content_1_0=ruleActivityContent
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getTriggerRule());
					}
					set(
						$current,
						"content",
						lv_content_1_0,
						"swt.most.statemachine.xtext.Parser.ActivityContent");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleGuard
entryRuleGuard returns [EObject current=null]:
	{ newCompositeNode(grammarAccess.getGuardRule()); }
	iv_ruleGuard=ruleGuard
	{ $current=$iv_ruleGuard.current; }
	EOF;

// Rule Guard
ruleGuard returns [EObject current=null]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		(
			{
				$current = forceCreateModelElement(
					grammarAccess.getGuardAccess().getGuardAction_0(),
					$current);
			}
		)
		(
			(
				{
					newCompositeNode(grammarAccess.getGuardAccess().getContentEStringParserRuleCall_1_0());
				}
				lv_content_1_0=ruleEString
				{
					if ($current==null) {
						$current = createModelElementForParent(grammarAccess.getGuardRule());
					}
					set(
						$current,
						"content",
						lv_content_1_0,
						"swt.most.statemachine.xtext.Parser.EString");
					afterParserOrEnumRuleCall();
				}
			)
		)
	)
;

// Entry rule entryRuleActivityContent
entryRuleActivityContent returns [String current=null]:
	{ newCompositeNode(grammarAccess.getActivityContentRule()); }
	iv_ruleActivityContent=ruleActivityContent
	{ $current=$iv_ruleActivityContent.current.getText(); }
	EOF;

// Rule ActivityContent
ruleActivityContent returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		this_STRING_0=RULE_STRING
		{
			$current.merge(this_STRING_0);
		}
		{
			newLeafNode(this_STRING_0, grammarAccess.getActivityContentAccess().getSTRINGTerminalRuleCall_0());
		}
		    |
		this_ID_1=RULE_ID
		{
			$current.merge(this_ID_1);
		}
		{
			newLeafNode(this_ID_1, grammarAccess.getActivityContentAccess().getIDTerminalRuleCall_1());
		}
		    |
		this_CALL_2=RULE_CALL
		{
			$current.merge(this_CALL_2);
		}
		{
			newLeafNode(this_CALL_2, grammarAccess.getActivityContentAccess().getCALLTerminalRuleCall_2());
		}
	)
;

// Entry rule entryRuleEString
entryRuleEString returns [String current=null]:
	{ newCompositeNode(grammarAccess.getEStringRule()); }
	iv_ruleEString=ruleEString
	{ $current=$iv_ruleEString.current.getText(); }
	EOF;

// Rule EString
ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()]
@init {
	enterRule();
}
@after {
	leaveRule();
}:
	(
		this_STRING_0=RULE_STRING
		{
			$current.merge(this_STRING_0);
		}
		{
			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
		}
		    |
		this_ID_1=RULE_ID
		{
			$current.merge(this_ID_1);
		}
		{
			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
		}
		    |
		this_INITIALSTATENAME_2=RULE_INITIALSTATENAME
		{
			$current.merge(this_INITIALSTATENAME_2);
		}
		{
			newLeafNode(this_INITIALSTATENAME_2, grammarAccess.getEStringAccess().getINITIALSTATENAMETerminalRuleCall_2());
		}
		    |
		this_FINALSTATENAME_3=RULE_FINALSTATENAME
		{
			$current.merge(this_FINALSTATENAME_3);
		}
		{
			newLeafNode(this_FINALSTATENAME_3, grammarAccess.getEStringAccess().getFINALSTATENAMETerminalRuleCall_3());
		}
	)
;

RULE_INITIALSTATENAME : 'I_' (RULE_INT|RULE_STRING|RULE_ID)+;

RULE_FINALSTATENAME : 'F_' (RULE_INT|RULE_STRING|RULE_ID)+;

RULE_CALL : RULE_ID ('.' RULE_ID)+;

RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

fragment RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' .|~(('\\'|'"')))* '"'|'\'' ('\\' .|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;
