package swt.most.statemachine.xtext.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import swt.most.statemachine.xtext.services.ParserGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalParserParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_FINALSTATENAME", "RULE_INITIALSTATENAME", "RULE_STRING", "RULE_ID", "RULE_CALL", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'StateMachine'", "'{'", "'}'", "'state'", "'entry:'", "'do:'", "'exit:'", "'end'", "'start'", "'-'", "'['", "']'", "'/'", "'->'"
    };
    public static final int RULE_INITIALSTATENAME=5;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=11;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_CALL=8;
    public static final int RULE_ID=7;
    public static final int RULE_WS=12;
    public static final int RULE_ANY_OTHER=13;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int RULE_FINALSTATENAME=4;
    public static final int RULE_INT=9;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=10;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalParserParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalParserParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalParserParser.tokenNames; }
    public String getGrammarFileName() { return "InternalParser.g"; }



     	private ParserGrammarAccess grammarAccess;

        public InternalParserParser(TokenStream input, ParserGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "StateMachine";
       	}

       	@Override
       	protected ParserGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleStateMachine"
    // InternalParser.g:64:1: entryRuleStateMachine returns [EObject current=null] : iv_ruleStateMachine= ruleStateMachine EOF ;
    public final EObject entryRuleStateMachine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateMachine = null;


        try {
            // InternalParser.g:64:53: (iv_ruleStateMachine= ruleStateMachine EOF )
            // InternalParser.g:65:2: iv_ruleStateMachine= ruleStateMachine EOF
            {
             newCompositeNode(grammarAccess.getStateMachineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateMachine=ruleStateMachine();

            state._fsp--;

             current =iv_ruleStateMachine; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateMachine"


    // $ANTLR start "ruleStateMachine"
    // InternalParser.g:71:1: ruleStateMachine returns [EObject current=null] : ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( (lv_initialstate_4_0= ruleInitialState ) ) ( ( (lv_states_5_0= ruleNormalState ) ) | ( (lv_finalstates_6_0= ruleFinalState ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )* otherlv_8= '}' ) ;
    public final EObject ruleStateMachine() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_initialstate_4_0 = null;

        EObject lv_states_5_0 = null;

        EObject lv_finalstates_6_0 = null;

        EObject lv_transitions_7_0 = null;



        	enterRule();

        try {
            // InternalParser.g:77:2: ( ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( (lv_initialstate_4_0= ruleInitialState ) ) ( ( (lv_states_5_0= ruleNormalState ) ) | ( (lv_finalstates_6_0= ruleFinalState ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )* otherlv_8= '}' ) )
            // InternalParser.g:78:2: ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( (lv_initialstate_4_0= ruleInitialState ) ) ( ( (lv_states_5_0= ruleNormalState ) ) | ( (lv_finalstates_6_0= ruleFinalState ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )* otherlv_8= '}' )
            {
            // InternalParser.g:78:2: ( () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( (lv_initialstate_4_0= ruleInitialState ) ) ( ( (lv_states_5_0= ruleNormalState ) ) | ( (lv_finalstates_6_0= ruleFinalState ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )* otherlv_8= '}' )
            // InternalParser.g:79:3: () otherlv_1= 'StateMachine' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( (lv_initialstate_4_0= ruleInitialState ) ) ( ( (lv_states_5_0= ruleNormalState ) ) | ( (lv_finalstates_6_0= ruleFinalState ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )* otherlv_8= '}'
            {
            // InternalParser.g:79:3: ()
            // InternalParser.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStateMachineAccess().getStateMachineAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getStateMachineAccess().getStateMachineKeyword_1());
            		
            // InternalParser.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalParser.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalParser.g:91:4: (lv_name_2_0= ruleEString )
            // InternalParser.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStateMachineAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"swt.most.statemachine.xtext.Parser.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getStateMachineAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalParser.g:113:3: ( (lv_initialstate_4_0= ruleInitialState ) )
            // InternalParser.g:114:4: (lv_initialstate_4_0= ruleInitialState )
            {
            // InternalParser.g:114:4: (lv_initialstate_4_0= ruleInitialState )
            // InternalParser.g:115:5: lv_initialstate_4_0= ruleInitialState
            {

            					newCompositeNode(grammarAccess.getStateMachineAccess().getInitialstateInitialStateParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_6);
            lv_initialstate_4_0=ruleInitialState();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateMachineRule());
            					}
            					set(
            						current,
            						"initialstate",
            						lv_initialstate_4_0,
            						"swt.most.statemachine.xtext.Parser.InitialState");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalParser.g:132:3: ( ( (lv_states_5_0= ruleNormalState ) ) | ( (lv_finalstates_6_0= ruleFinalState ) ) | ( (lv_transitions_7_0= ruleTransition ) ) )*
            loop1:
            do {
                int alt1=4;
                switch ( input.LA(1) ) {
                case 17:
                    {
                    alt1=1;
                    }
                    break;
                case 21:
                    {
                    alt1=2;
                    }
                    break;
                case RULE_FINALSTATENAME:
                case RULE_INITIALSTATENAME:
                case RULE_STRING:
                case RULE_ID:
                    {
                    alt1=3;
                    }
                    break;

                }

                switch (alt1) {
            	case 1 :
            	    // InternalParser.g:133:4: ( (lv_states_5_0= ruleNormalState ) )
            	    {
            	    // InternalParser.g:133:4: ( (lv_states_5_0= ruleNormalState ) )
            	    // InternalParser.g:134:5: (lv_states_5_0= ruleNormalState )
            	    {
            	    // InternalParser.g:134:5: (lv_states_5_0= ruleNormalState )
            	    // InternalParser.g:135:6: lv_states_5_0= ruleNormalState
            	    {

            	    						newCompositeNode(grammarAccess.getStateMachineAccess().getStatesNormalStateParserRuleCall_5_0_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_states_5_0=ruleNormalState();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"states",
            	    							lv_states_5_0,
            	    							"swt.most.statemachine.xtext.Parser.NormalState");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalParser.g:153:4: ( (lv_finalstates_6_0= ruleFinalState ) )
            	    {
            	    // InternalParser.g:153:4: ( (lv_finalstates_6_0= ruleFinalState ) )
            	    // InternalParser.g:154:5: (lv_finalstates_6_0= ruleFinalState )
            	    {
            	    // InternalParser.g:154:5: (lv_finalstates_6_0= ruleFinalState )
            	    // InternalParser.g:155:6: lv_finalstates_6_0= ruleFinalState
            	    {

            	    						newCompositeNode(grammarAccess.getStateMachineAccess().getFinalstatesFinalStateParserRuleCall_5_1_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_finalstates_6_0=ruleFinalState();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"finalstates",
            	    							lv_finalstates_6_0,
            	    							"swt.most.statemachine.xtext.Parser.FinalState");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalParser.g:173:4: ( (lv_transitions_7_0= ruleTransition ) )
            	    {
            	    // InternalParser.g:173:4: ( (lv_transitions_7_0= ruleTransition ) )
            	    // InternalParser.g:174:5: (lv_transitions_7_0= ruleTransition )
            	    {
            	    // InternalParser.g:174:5: (lv_transitions_7_0= ruleTransition )
            	    // InternalParser.g:175:6: lv_transitions_7_0= ruleTransition
            	    {

            	    						newCompositeNode(grammarAccess.getStateMachineAccess().getTransitionsTransitionParserRuleCall_5_2_0());
            	    					
            	    pushFollow(FOLLOW_6);
            	    lv_transitions_7_0=ruleTransition();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStateMachineRule());
            	    						}
            	    						add(
            	    							current,
            	    							"transitions",
            	    							lv_transitions_7_0,
            	    							"swt.most.statemachine.xtext.Parser.Transition");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getStateMachineAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateMachine"


    // $ANTLR start "entryRuleNormalState"
    // InternalParser.g:201:1: entryRuleNormalState returns [EObject current=null] : iv_ruleNormalState= ruleNormalState EOF ;
    public final EObject entryRuleNormalState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNormalState = null;


        try {
            // InternalParser.g:201:52: (iv_ruleNormalState= ruleNormalState EOF )
            // InternalParser.g:202:2: iv_ruleNormalState= ruleNormalState EOF
            {
             newCompositeNode(grammarAccess.getNormalStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNormalState=ruleNormalState();

            state._fsp--;

             current =iv_ruleNormalState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNormalState"


    // $ANTLR start "ruleNormalState"
    // InternalParser.g:208:1: ruleNormalState returns [EObject current=null] : ( () otherlv_1= 'state' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}' )? ) ;
    public final EObject ruleNormalState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_entry__5_0 = null;

        EObject lv_do__7_0 = null;

        EObject lv_exit__9_0 = null;



        	enterRule();

        try {
            // InternalParser.g:214:2: ( ( () otherlv_1= 'state' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}' )? ) )
            // InternalParser.g:215:2: ( () otherlv_1= 'state' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}' )? )
            {
            // InternalParser.g:215:2: ( () otherlv_1= 'state' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}' )? )
            // InternalParser.g:216:3: () otherlv_1= 'state' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}' )?
            {
            // InternalParser.g:216:3: ()
            // InternalParser.g:217:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNormalStateAccess().getNormalStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getNormalStateAccess().getStateKeyword_1());
            		
            // InternalParser.g:227:3: ( (lv_name_2_0= ruleEString ) )
            // InternalParser.g:228:4: (lv_name_2_0= ruleEString )
            {
            // InternalParser.g:228:4: (lv_name_2_0= ruleEString )
            // InternalParser.g:229:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getNormalStateAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_7);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNormalStateRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"swt.most.statemachine.xtext.Parser.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalParser.g:246:3: (otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalParser.g:247:4: otherlv_3= '{' (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )? (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )? (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )? otherlv_10= '}'
                    {
                    otherlv_3=(Token)match(input,15,FOLLOW_8); 

                    				newLeafNode(otherlv_3, grammarAccess.getNormalStateAccess().getLeftCurlyBracketKeyword_3_0());
                    			
                    // InternalParser.g:251:4: (otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) ) )?
                    int alt2=2;
                    int LA2_0 = input.LA(1);

                    if ( (LA2_0==18) ) {
                        alt2=1;
                    }
                    switch (alt2) {
                        case 1 :
                            // InternalParser.g:252:5: otherlv_4= 'entry:' ( (lv_entry__5_0= ruleAction ) )
                            {
                            otherlv_4=(Token)match(input,18,FOLLOW_9); 

                            					newLeafNode(otherlv_4, grammarAccess.getNormalStateAccess().getEntryKeyword_3_1_0());
                            				
                            // InternalParser.g:256:5: ( (lv_entry__5_0= ruleAction ) )
                            // InternalParser.g:257:6: (lv_entry__5_0= ruleAction )
                            {
                            // InternalParser.g:257:6: (lv_entry__5_0= ruleAction )
                            // InternalParser.g:258:7: lv_entry__5_0= ruleAction
                            {

                            							newCompositeNode(grammarAccess.getNormalStateAccess().getEntry_ActionParserRuleCall_3_1_1_0());
                            						
                            pushFollow(FOLLOW_10);
                            lv_entry__5_0=ruleAction();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getNormalStateRule());
                            							}
                            							set(
                            								current,
                            								"entry_",
                            								lv_entry__5_0,
                            								"swt.most.statemachine.xtext.Parser.Action");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }

                    // InternalParser.g:276:4: (otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) ) )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==19) ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // InternalParser.g:277:5: otherlv_6= 'do:' ( (lv_do__7_0= ruleAction ) )
                            {
                            otherlv_6=(Token)match(input,19,FOLLOW_9); 

                            					newLeafNode(otherlv_6, grammarAccess.getNormalStateAccess().getDoKeyword_3_2_0());
                            				
                            // InternalParser.g:281:5: ( (lv_do__7_0= ruleAction ) )
                            // InternalParser.g:282:6: (lv_do__7_0= ruleAction )
                            {
                            // InternalParser.g:282:6: (lv_do__7_0= ruleAction )
                            // InternalParser.g:283:7: lv_do__7_0= ruleAction
                            {

                            							newCompositeNode(grammarAccess.getNormalStateAccess().getDo_ActionParserRuleCall_3_2_1_0());
                            						
                            pushFollow(FOLLOW_11);
                            lv_do__7_0=ruleAction();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getNormalStateRule());
                            							}
                            							set(
                            								current,
                            								"do_",
                            								lv_do__7_0,
                            								"swt.most.statemachine.xtext.Parser.Action");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }

                    // InternalParser.g:301:4: (otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) ) )?
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==20) ) {
                        alt4=1;
                    }
                    switch (alt4) {
                        case 1 :
                            // InternalParser.g:302:5: otherlv_8= 'exit:' ( (lv_exit__9_0= ruleAction ) )
                            {
                            otherlv_8=(Token)match(input,20,FOLLOW_9); 

                            					newLeafNode(otherlv_8, grammarAccess.getNormalStateAccess().getExitKeyword_3_3_0());
                            				
                            // InternalParser.g:306:5: ( (lv_exit__9_0= ruleAction ) )
                            // InternalParser.g:307:6: (lv_exit__9_0= ruleAction )
                            {
                            // InternalParser.g:307:6: (lv_exit__9_0= ruleAction )
                            // InternalParser.g:308:7: lv_exit__9_0= ruleAction
                            {

                            							newCompositeNode(grammarAccess.getNormalStateAccess().getExit_ActionParserRuleCall_3_3_1_0());
                            						
                            pushFollow(FOLLOW_12);
                            lv_exit__9_0=ruleAction();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getNormalStateRule());
                            							}
                            							set(
                            								current,
                            								"exit_",
                            								lv_exit__9_0,
                            								"swt.most.statemachine.xtext.Parser.Action");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,16,FOLLOW_2); 

                    				newLeafNode(otherlv_10, grammarAccess.getNormalStateAccess().getRightCurlyBracketKeyword_3_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNormalState"


    // $ANTLR start "entryRuleFinalState"
    // InternalParser.g:335:1: entryRuleFinalState returns [EObject current=null] : iv_ruleFinalState= ruleFinalState EOF ;
    public final EObject entryRuleFinalState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFinalState = null;


        try {
            // InternalParser.g:335:51: (iv_ruleFinalState= ruleFinalState EOF )
            // InternalParser.g:336:2: iv_ruleFinalState= ruleFinalState EOF
            {
             newCompositeNode(grammarAccess.getFinalStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFinalState=ruleFinalState();

            state._fsp--;

             current =iv_ruleFinalState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFinalState"


    // $ANTLR start "ruleFinalState"
    // InternalParser.g:342:1: ruleFinalState returns [EObject current=null] : ( () otherlv_1= 'end' ( (lv_name_2_0= RULE_FINALSTATENAME ) ) ) ;
    public final EObject ruleFinalState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalParser.g:348:2: ( ( () otherlv_1= 'end' ( (lv_name_2_0= RULE_FINALSTATENAME ) ) ) )
            // InternalParser.g:349:2: ( () otherlv_1= 'end' ( (lv_name_2_0= RULE_FINALSTATENAME ) ) )
            {
            // InternalParser.g:349:2: ( () otherlv_1= 'end' ( (lv_name_2_0= RULE_FINALSTATENAME ) ) )
            // InternalParser.g:350:3: () otherlv_1= 'end' ( (lv_name_2_0= RULE_FINALSTATENAME ) )
            {
            // InternalParser.g:350:3: ()
            // InternalParser.g:351:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFinalStateAccess().getFinalStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,21,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getFinalStateAccess().getEndKeyword_1());
            		
            // InternalParser.g:361:3: ( (lv_name_2_0= RULE_FINALSTATENAME ) )
            // InternalParser.g:362:4: (lv_name_2_0= RULE_FINALSTATENAME )
            {
            // InternalParser.g:362:4: (lv_name_2_0= RULE_FINALSTATENAME )
            // InternalParser.g:363:5: lv_name_2_0= RULE_FINALSTATENAME
            {
            lv_name_2_0=(Token)match(input,RULE_FINALSTATENAME,FOLLOW_2); 

            					newLeafNode(lv_name_2_0, grammarAccess.getFinalStateAccess().getNameFINALSTATENAMETerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFinalStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"swt.most.statemachine.xtext.Parser.FINALSTATENAME");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFinalState"


    // $ANTLR start "entryRuleInitialState"
    // InternalParser.g:383:1: entryRuleInitialState returns [EObject current=null] : iv_ruleInitialState= ruleInitialState EOF ;
    public final EObject entryRuleInitialState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInitialState = null;


        try {
            // InternalParser.g:383:53: (iv_ruleInitialState= ruleInitialState EOF )
            // InternalParser.g:384:2: iv_ruleInitialState= ruleInitialState EOF
            {
             newCompositeNode(grammarAccess.getInitialStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInitialState=ruleInitialState();

            state._fsp--;

             current =iv_ruleInitialState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInitialState"


    // $ANTLR start "ruleInitialState"
    // InternalParser.g:390:1: ruleInitialState returns [EObject current=null] : ( () otherlv_1= 'start' ( (lv_name_2_0= RULE_INITIALSTATENAME ) ) ) ;
    public final EObject ruleInitialState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;


        	enterRule();

        try {
            // InternalParser.g:396:2: ( ( () otherlv_1= 'start' ( (lv_name_2_0= RULE_INITIALSTATENAME ) ) ) )
            // InternalParser.g:397:2: ( () otherlv_1= 'start' ( (lv_name_2_0= RULE_INITIALSTATENAME ) ) )
            {
            // InternalParser.g:397:2: ( () otherlv_1= 'start' ( (lv_name_2_0= RULE_INITIALSTATENAME ) ) )
            // InternalParser.g:398:3: () otherlv_1= 'start' ( (lv_name_2_0= RULE_INITIALSTATENAME ) )
            {
            // InternalParser.g:398:3: ()
            // InternalParser.g:399:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInitialStateAccess().getInitialStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,22,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getInitialStateAccess().getStartKeyword_1());
            		
            // InternalParser.g:409:3: ( (lv_name_2_0= RULE_INITIALSTATENAME ) )
            // InternalParser.g:410:4: (lv_name_2_0= RULE_INITIALSTATENAME )
            {
            // InternalParser.g:410:4: (lv_name_2_0= RULE_INITIALSTATENAME )
            // InternalParser.g:411:5: lv_name_2_0= RULE_INITIALSTATENAME
            {
            lv_name_2_0=(Token)match(input,RULE_INITIALSTATENAME,FOLLOW_2); 

            					newLeafNode(lv_name_2_0, grammarAccess.getInitialStateAccess().getNameINITIALSTATENAMETerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInitialStateRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"swt.most.statemachine.xtext.Parser.INITIALSTATENAME");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInitialState"


    // $ANTLR start "entryRuleTransition"
    // InternalParser.g:431:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // InternalParser.g:431:51: (iv_ruleTransition= ruleTransition EOF )
            // InternalParser.g:432:2: iv_ruleTransition= ruleTransition EOF
            {
             newCompositeNode(grammarAccess.getTransitionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTransition=ruleTransition();

            state._fsp--;

             current =iv_ruleTransition; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // InternalParser.g:438:1: ruleTransition returns [EObject current=null] : ( () ( ( ruleEString ) ) otherlv_2= '-' ( (lv_trigger_3_0= ruleTrigger ) )? (otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']' )? (otherlv_7= '/' ( (lv_action_8_0= ruleAction ) ) )? otherlv_9= '->' ( ( ruleEString ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_trigger_3_0 = null;

        EObject lv_guard_5_0 = null;

        EObject lv_action_8_0 = null;



        	enterRule();

        try {
            // InternalParser.g:444:2: ( ( () ( ( ruleEString ) ) otherlv_2= '-' ( (lv_trigger_3_0= ruleTrigger ) )? (otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']' )? (otherlv_7= '/' ( (lv_action_8_0= ruleAction ) ) )? otherlv_9= '->' ( ( ruleEString ) ) ) )
            // InternalParser.g:445:2: ( () ( ( ruleEString ) ) otherlv_2= '-' ( (lv_trigger_3_0= ruleTrigger ) )? (otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']' )? (otherlv_7= '/' ( (lv_action_8_0= ruleAction ) ) )? otherlv_9= '->' ( ( ruleEString ) ) )
            {
            // InternalParser.g:445:2: ( () ( ( ruleEString ) ) otherlv_2= '-' ( (lv_trigger_3_0= ruleTrigger ) )? (otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']' )? (otherlv_7= '/' ( (lv_action_8_0= ruleAction ) ) )? otherlv_9= '->' ( ( ruleEString ) ) )
            // InternalParser.g:446:3: () ( ( ruleEString ) ) otherlv_2= '-' ( (lv_trigger_3_0= ruleTrigger ) )? (otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']' )? (otherlv_7= '/' ( (lv_action_8_0= ruleAction ) ) )? otherlv_9= '->' ( ( ruleEString ) )
            {
            // InternalParser.g:446:3: ()
            // InternalParser.g:447:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTransitionAccess().getTransitionAction_0(),
            					current);
            			

            }

            // InternalParser.g:453:3: ( ( ruleEString ) )
            // InternalParser.g:454:4: ( ruleEString )
            {
            // InternalParser.g:454:4: ( ruleEString )
            // InternalParser.g:455:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getFromStateCrossReference_1_0());
            				
            pushFollow(FOLLOW_15);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getHyphenMinusKeyword_2());
            		
            // InternalParser.g:473:3: ( (lv_trigger_3_0= ruleTrigger ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_CALL)) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalParser.g:474:4: (lv_trigger_3_0= ruleTrigger )
                    {
                    // InternalParser.g:474:4: (lv_trigger_3_0= ruleTrigger )
                    // InternalParser.g:475:5: lv_trigger_3_0= ruleTrigger
                    {

                    					newCompositeNode(grammarAccess.getTransitionAccess().getTriggerTriggerParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_17);
                    lv_trigger_3_0=ruleTrigger();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getTransitionRule());
                    					}
                    					set(
                    						current,
                    						"trigger",
                    						lv_trigger_3_0,
                    						"swt.most.statemachine.xtext.Parser.Trigger");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalParser.g:492:3: (otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==24) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalParser.g:493:4: otherlv_4= '[' ( (lv_guard_5_0= ruleGuard ) ) otherlv_6= ']'
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getLeftSquareBracketKeyword_4_0());
                    			
                    // InternalParser.g:497:4: ( (lv_guard_5_0= ruleGuard ) )
                    // InternalParser.g:498:5: (lv_guard_5_0= ruleGuard )
                    {
                    // InternalParser.g:498:5: (lv_guard_5_0= ruleGuard )
                    // InternalParser.g:499:6: lv_guard_5_0= ruleGuard
                    {

                    						newCompositeNode(grammarAccess.getTransitionAccess().getGuardGuardParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_guard_5_0=ruleGuard();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTransitionRule());
                    						}
                    						set(
                    							current,
                    							"guard",
                    							lv_guard_5_0,
                    							"swt.most.statemachine.xtext.Parser.Guard");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,25,FOLLOW_19); 

                    				newLeafNode(otherlv_6, grammarAccess.getTransitionAccess().getRightSquareBracketKeyword_4_2());
                    			

                    }
                    break;

            }

            // InternalParser.g:521:3: (otherlv_7= '/' ( (lv_action_8_0= ruleAction ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==26) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalParser.g:522:4: otherlv_7= '/' ( (lv_action_8_0= ruleAction ) )
                    {
                    otherlv_7=(Token)match(input,26,FOLLOW_9); 

                    				newLeafNode(otherlv_7, grammarAccess.getTransitionAccess().getSolidusKeyword_5_0());
                    			
                    // InternalParser.g:526:4: ( (lv_action_8_0= ruleAction ) )
                    // InternalParser.g:527:5: (lv_action_8_0= ruleAction )
                    {
                    // InternalParser.g:527:5: (lv_action_8_0= ruleAction )
                    // InternalParser.g:528:6: lv_action_8_0= ruleAction
                    {

                    						newCompositeNode(grammarAccess.getTransitionAccess().getActionActionParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_action_8_0=ruleAction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTransitionRule());
                    						}
                    						set(
                    							current,
                    							"action",
                    							lv_action_8_0,
                    							"swt.most.statemachine.xtext.Parser.Action");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,27,FOLLOW_3); 

            			newLeafNode(otherlv_9, grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_6());
            		
            // InternalParser.g:550:3: ( ( ruleEString ) )
            // InternalParser.g:551:4: ( ruleEString )
            {
            // InternalParser.g:551:4: ( ruleEString )
            // InternalParser.g:552:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTransitionRule());
            					}
            				

            					newCompositeNode(grammarAccess.getTransitionAccess().getToStateCrossReference_7_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleAction"
    // InternalParser.g:570:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalParser.g:570:47: (iv_ruleAction= ruleAction EOF )
            // InternalParser.g:571:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalParser.g:577:1: ruleAction returns [EObject current=null] : ( () ( (lv_content_1_0= ruleActivityContent ) ) ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_content_1_0 = null;



        	enterRule();

        try {
            // InternalParser.g:583:2: ( ( () ( (lv_content_1_0= ruleActivityContent ) ) ) )
            // InternalParser.g:584:2: ( () ( (lv_content_1_0= ruleActivityContent ) ) )
            {
            // InternalParser.g:584:2: ( () ( (lv_content_1_0= ruleActivityContent ) ) )
            // InternalParser.g:585:3: () ( (lv_content_1_0= ruleActivityContent ) )
            {
            // InternalParser.g:585:3: ()
            // InternalParser.g:586:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getActionAccess().getActionAction_0(),
            					current);
            			

            }

            // InternalParser.g:592:3: ( (lv_content_1_0= ruleActivityContent ) )
            // InternalParser.g:593:4: (lv_content_1_0= ruleActivityContent )
            {
            // InternalParser.g:593:4: (lv_content_1_0= ruleActivityContent )
            // InternalParser.g:594:5: lv_content_1_0= ruleActivityContent
            {

            					newCompositeNode(grammarAccess.getActionAccess().getContentActivityContentParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_content_1_0=ruleActivityContent();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getActionRule());
            					}
            					set(
            						current,
            						"content",
            						lv_content_1_0,
            						"swt.most.statemachine.xtext.Parser.ActivityContent");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleTrigger"
    // InternalParser.g:615:1: entryRuleTrigger returns [EObject current=null] : iv_ruleTrigger= ruleTrigger EOF ;
    public final EObject entryRuleTrigger() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTrigger = null;


        try {
            // InternalParser.g:615:48: (iv_ruleTrigger= ruleTrigger EOF )
            // InternalParser.g:616:2: iv_ruleTrigger= ruleTrigger EOF
            {
             newCompositeNode(grammarAccess.getTriggerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTrigger=ruleTrigger();

            state._fsp--;

             current =iv_ruleTrigger; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTrigger"


    // $ANTLR start "ruleTrigger"
    // InternalParser.g:622:1: ruleTrigger returns [EObject current=null] : ( () ( (lv_content_1_0= ruleActivityContent ) ) ) ;
    public final EObject ruleTrigger() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_content_1_0 = null;



        	enterRule();

        try {
            // InternalParser.g:628:2: ( ( () ( (lv_content_1_0= ruleActivityContent ) ) ) )
            // InternalParser.g:629:2: ( () ( (lv_content_1_0= ruleActivityContent ) ) )
            {
            // InternalParser.g:629:2: ( () ( (lv_content_1_0= ruleActivityContent ) ) )
            // InternalParser.g:630:3: () ( (lv_content_1_0= ruleActivityContent ) )
            {
            // InternalParser.g:630:3: ()
            // InternalParser.g:631:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTriggerAccess().getTriggerAction_0(),
            					current);
            			

            }

            // InternalParser.g:637:3: ( (lv_content_1_0= ruleActivityContent ) )
            // InternalParser.g:638:4: (lv_content_1_0= ruleActivityContent )
            {
            // InternalParser.g:638:4: (lv_content_1_0= ruleActivityContent )
            // InternalParser.g:639:5: lv_content_1_0= ruleActivityContent
            {

            					newCompositeNode(grammarAccess.getTriggerAccess().getContentActivityContentParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_content_1_0=ruleActivityContent();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTriggerRule());
            					}
            					set(
            						current,
            						"content",
            						lv_content_1_0,
            						"swt.most.statemachine.xtext.Parser.ActivityContent");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTrigger"


    // $ANTLR start "entryRuleGuard"
    // InternalParser.g:660:1: entryRuleGuard returns [EObject current=null] : iv_ruleGuard= ruleGuard EOF ;
    public final EObject entryRuleGuard() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGuard = null;


        try {
            // InternalParser.g:660:46: (iv_ruleGuard= ruleGuard EOF )
            // InternalParser.g:661:2: iv_ruleGuard= ruleGuard EOF
            {
             newCompositeNode(grammarAccess.getGuardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGuard=ruleGuard();

            state._fsp--;

             current =iv_ruleGuard; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGuard"


    // $ANTLR start "ruleGuard"
    // InternalParser.g:667:1: ruleGuard returns [EObject current=null] : ( () ( (lv_content_1_0= ruleEString ) ) ) ;
    public final EObject ruleGuard() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_content_1_0 = null;



        	enterRule();

        try {
            // InternalParser.g:673:2: ( ( () ( (lv_content_1_0= ruleEString ) ) ) )
            // InternalParser.g:674:2: ( () ( (lv_content_1_0= ruleEString ) ) )
            {
            // InternalParser.g:674:2: ( () ( (lv_content_1_0= ruleEString ) ) )
            // InternalParser.g:675:3: () ( (lv_content_1_0= ruleEString ) )
            {
            // InternalParser.g:675:3: ()
            // InternalParser.g:676:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGuardAccess().getGuardAction_0(),
            					current);
            			

            }

            // InternalParser.g:682:3: ( (lv_content_1_0= ruleEString ) )
            // InternalParser.g:683:4: (lv_content_1_0= ruleEString )
            {
            // InternalParser.g:683:4: (lv_content_1_0= ruleEString )
            // InternalParser.g:684:5: lv_content_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getGuardAccess().getContentEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_content_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGuardRule());
            					}
            					set(
            						current,
            						"content",
            						lv_content_1_0,
            						"swt.most.statemachine.xtext.Parser.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGuard"


    // $ANTLR start "entryRuleActivityContent"
    // InternalParser.g:705:1: entryRuleActivityContent returns [String current=null] : iv_ruleActivityContent= ruleActivityContent EOF ;
    public final String entryRuleActivityContent() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleActivityContent = null;


        try {
            // InternalParser.g:705:55: (iv_ruleActivityContent= ruleActivityContent EOF )
            // InternalParser.g:706:2: iv_ruleActivityContent= ruleActivityContent EOF
            {
             newCompositeNode(grammarAccess.getActivityContentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleActivityContent=ruleActivityContent();

            state._fsp--;

             current =iv_ruleActivityContent.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleActivityContent"


    // $ANTLR start "ruleActivityContent"
    // InternalParser.g:712:1: ruleActivityContent returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_CALL_2= RULE_CALL ) ;
    public final AntlrDatatypeRuleToken ruleActivityContent() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;
        Token this_CALL_2=null;


        	enterRule();

        try {
            // InternalParser.g:718:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_CALL_2= RULE_CALL ) )
            // InternalParser.g:719:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_CALL_2= RULE_CALL )
            {
            // InternalParser.g:719:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_CALL_2= RULE_CALL )
            int alt9=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt9=1;
                }
                break;
            case RULE_ID:
                {
                alt9=2;
                }
                break;
            case RULE_CALL:
                {
                alt9=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalParser.g:720:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getActivityContentAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalParser.g:728:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getActivityContentAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalParser.g:736:3: this_CALL_2= RULE_CALL
                    {
                    this_CALL_2=(Token)match(input,RULE_CALL,FOLLOW_2); 

                    			current.merge(this_CALL_2);
                    		

                    			newLeafNode(this_CALL_2, grammarAccess.getActivityContentAccess().getCALLTerminalRuleCall_2());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleActivityContent"


    // $ANTLR start "entryRuleEString"
    // InternalParser.g:747:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalParser.g:747:47: (iv_ruleEString= ruleEString EOF )
            // InternalParser.g:748:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalParser.g:754:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_INITIALSTATENAME_2= RULE_INITIALSTATENAME | this_FINALSTATENAME_3= RULE_FINALSTATENAME ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;
        Token this_INITIALSTATENAME_2=null;
        Token this_FINALSTATENAME_3=null;


        	enterRule();

        try {
            // InternalParser.g:760:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_INITIALSTATENAME_2= RULE_INITIALSTATENAME | this_FINALSTATENAME_3= RULE_FINALSTATENAME ) )
            // InternalParser.g:761:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_INITIALSTATENAME_2= RULE_INITIALSTATENAME | this_FINALSTATENAME_3= RULE_FINALSTATENAME )
            {
            // InternalParser.g:761:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID | this_INITIALSTATENAME_2= RULE_INITIALSTATENAME | this_FINALSTATENAME_3= RULE_FINALSTATENAME )
            int alt10=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt10=1;
                }
                break;
            case RULE_ID:
                {
                alt10=2;
                }
                break;
            case RULE_INITIALSTATENAME:
                {
                alt10=3;
                }
                break;
            case RULE_FINALSTATENAME:
                {
                alt10=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalParser.g:762:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalParser.g:770:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalParser.g:778:3: this_INITIALSTATENAME_2= RULE_INITIALSTATENAME
                    {
                    this_INITIALSTATENAME_2=(Token)match(input,RULE_INITIALSTATENAME,FOLLOW_2); 

                    			current.merge(this_INITIALSTATENAME_2);
                    		

                    			newLeafNode(this_INITIALSTATENAME_2, grammarAccess.getEStringAccess().getINITIALSTATENAMETerminalRuleCall_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalParser.g:786:3: this_FINALSTATENAME_3= RULE_FINALSTATENAME
                    {
                    this_FINALSTATENAME_3=(Token)match(input,RULE_FINALSTATENAME,FOLLOW_2); 

                    			current.merge(this_FINALSTATENAME_3);
                    		

                    			newLeafNode(this_FINALSTATENAME_3, grammarAccess.getEStringAccess().getFINALSTATENAMETerminalRuleCall_3());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000000F0L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000000002300F0L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000000001D0000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000000000001C0L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000190000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000110000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000000D0001C0L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x000000000D000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x000000000C000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000008000000L});

}