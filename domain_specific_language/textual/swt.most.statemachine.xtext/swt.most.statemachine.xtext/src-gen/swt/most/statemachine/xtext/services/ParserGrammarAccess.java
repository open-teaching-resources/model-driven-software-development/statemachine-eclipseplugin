/*
 * generated by Xtext 2.35.0
 */
package swt.most.statemachine.xtext.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class ParserGrammarAccess extends AbstractElementFinder.AbstractGrammarElementFinder {
	
	public class StateMachineElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.StateMachine");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cStateMachineAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cStateMachineKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameEStringParserRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Keyword cLeftCurlyBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Assignment cInitialstateAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cInitialstateInitialStateParserRuleCall_4_0 = (RuleCall)cInitialstateAssignment_4.eContents().get(0);
		private final Alternatives cAlternatives_5 = (Alternatives)cGroup.eContents().get(5);
		private final Assignment cStatesAssignment_5_0 = (Assignment)cAlternatives_5.eContents().get(0);
		private final RuleCall cStatesNormalStateParserRuleCall_5_0_0 = (RuleCall)cStatesAssignment_5_0.eContents().get(0);
		private final Assignment cFinalstatesAssignment_5_1 = (Assignment)cAlternatives_5.eContents().get(1);
		private final RuleCall cFinalstatesFinalStateParserRuleCall_5_1_0 = (RuleCall)cFinalstatesAssignment_5_1.eContents().get(0);
		private final Assignment cTransitionsAssignment_5_2 = (Assignment)cAlternatives_5.eContents().get(2);
		private final RuleCall cTransitionsTransitionParserRuleCall_5_2_0 = (RuleCall)cTransitionsAssignment_5_2.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_6 = (Keyword)cGroup.eContents().get(6);
		
		//StateMachine returns StateMachine:
		//    {StateMachine}
		//    'StateMachine' name=EString
		//    '{'
		//        initialstate=InitialState
		//        ( states+=NormalState | finalstates+=FinalState | transitions+=Transition)*
		//    '}';
		@Override public ParserRule getRule() { return rule; }
		
		//{StateMachine}
		//'StateMachine' name=EString
		//'{'
		//    initialstate=InitialState
		//    ( states+=NormalState | finalstates+=FinalState | transitions+=Transition)*
		//'}'
		public Group getGroup() { return cGroup; }
		
		//{StateMachine}
		public Action getStateMachineAction_0() { return cStateMachineAction_0; }
		
		//'StateMachine'
		public Keyword getStateMachineKeyword_1() { return cStateMachineKeyword_1; }
		
		//name=EString
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//EString
		public RuleCall getNameEStringParserRuleCall_2_0() { return cNameEStringParserRuleCall_2_0; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_3() { return cLeftCurlyBracketKeyword_3; }
		
		//initialstate=InitialState
		public Assignment getInitialstateAssignment_4() { return cInitialstateAssignment_4; }
		
		//InitialState
		public RuleCall getInitialstateInitialStateParserRuleCall_4_0() { return cInitialstateInitialStateParserRuleCall_4_0; }
		
		//( states+=NormalState | finalstates+=FinalState | transitions+=Transition)*
		public Alternatives getAlternatives_5() { return cAlternatives_5; }
		
		//states+=NormalState
		public Assignment getStatesAssignment_5_0() { return cStatesAssignment_5_0; }
		
		//NormalState
		public RuleCall getStatesNormalStateParserRuleCall_5_0_0() { return cStatesNormalStateParserRuleCall_5_0_0; }
		
		//finalstates+=FinalState
		public Assignment getFinalstatesAssignment_5_1() { return cFinalstatesAssignment_5_1; }
		
		//FinalState
		public RuleCall getFinalstatesFinalStateParserRuleCall_5_1_0() { return cFinalstatesFinalStateParserRuleCall_5_1_0; }
		
		//transitions+=Transition
		public Assignment getTransitionsAssignment_5_2() { return cTransitionsAssignment_5_2; }
		
		//Transition
		public RuleCall getTransitionsTransitionParserRuleCall_5_2_0() { return cTransitionsTransitionParserRuleCall_5_2_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_6() { return cRightCurlyBracketKeyword_6; }
	}
	public class StateElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.State");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cInitialStateParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cFinalStateParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cNormalStateParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		
		//State returns State:
		//    InitialState | FinalState | NormalState;
		@Override public ParserRule getRule() { return rule; }
		
		//InitialState | FinalState | NormalState
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//InitialState
		public RuleCall getInitialStateParserRuleCall_0() { return cInitialStateParserRuleCall_0; }
		
		//FinalState
		public RuleCall getFinalStateParserRuleCall_1() { return cFinalStateParserRuleCall_1; }
		
		//NormalState
		public RuleCall getNormalStateParserRuleCall_2() { return cNormalStateParserRuleCall_2; }
	}
	public class NormalStateElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.NormalState");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cNormalStateAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cStateKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameEStringParserRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cLeftCurlyBracketKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Group cGroup_3_1 = (Group)cGroup_3.eContents().get(1);
		private final Keyword cEntryKeyword_3_1_0 = (Keyword)cGroup_3_1.eContents().get(0);
		private final Assignment cEntry_Assignment_3_1_1 = (Assignment)cGroup_3_1.eContents().get(1);
		private final RuleCall cEntry_ActionParserRuleCall_3_1_1_0 = (RuleCall)cEntry_Assignment_3_1_1.eContents().get(0);
		private final Group cGroup_3_2 = (Group)cGroup_3.eContents().get(2);
		private final Keyword cDoKeyword_3_2_0 = (Keyword)cGroup_3_2.eContents().get(0);
		private final Assignment cDo_Assignment_3_2_1 = (Assignment)cGroup_3_2.eContents().get(1);
		private final RuleCall cDo_ActionParserRuleCall_3_2_1_0 = (RuleCall)cDo_Assignment_3_2_1.eContents().get(0);
		private final Group cGroup_3_3 = (Group)cGroup_3.eContents().get(3);
		private final Keyword cExitKeyword_3_3_0 = (Keyword)cGroup_3_3.eContents().get(0);
		private final Assignment cExit_Assignment_3_3_1 = (Assignment)cGroup_3_3.eContents().get(1);
		private final RuleCall cExit_ActionParserRuleCall_3_3_1_0 = (RuleCall)cExit_Assignment_3_3_1.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_3_4 = (Keyword)cGroup_3.eContents().get(4);
		
		//NormalState returns NormalState:
		//    {NormalState}
		//    'state' name=EString ('{'
		//        ('entry:' entry_=Action)?
		//        ('do:' do_=Action)?
		//        ('exit:' exit_=Action)?
		//    '}')?;
		@Override public ParserRule getRule() { return rule; }
		
		//{NormalState}
		//'state' name=EString ('{'
		//    ('entry:' entry_=Action)?
		//    ('do:' do_=Action)?
		//    ('exit:' exit_=Action)?
		//'}')?
		public Group getGroup() { return cGroup; }
		
		//{NormalState}
		public Action getNormalStateAction_0() { return cNormalStateAction_0; }
		
		//'state'
		public Keyword getStateKeyword_1() { return cStateKeyword_1; }
		
		//name=EString
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//EString
		public RuleCall getNameEStringParserRuleCall_2_0() { return cNameEStringParserRuleCall_2_0; }
		
		//('{'
		//       ('entry:' entry_=Action)?
		//       ('do:' do_=Action)?
		//       ('exit:' exit_=Action)?
		//   '}')?
		public Group getGroup_3() { return cGroup_3; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_3_0() { return cLeftCurlyBracketKeyword_3_0; }
		
		//('entry:' entry_=Action)?
		public Group getGroup_3_1() { return cGroup_3_1; }
		
		//'entry:'
		public Keyword getEntryKeyword_3_1_0() { return cEntryKeyword_3_1_0; }
		
		//entry_=Action
		public Assignment getEntry_Assignment_3_1_1() { return cEntry_Assignment_3_1_1; }
		
		//Action
		public RuleCall getEntry_ActionParserRuleCall_3_1_1_0() { return cEntry_ActionParserRuleCall_3_1_1_0; }
		
		//('do:' do_=Action)?
		public Group getGroup_3_2() { return cGroup_3_2; }
		
		//'do:'
		public Keyword getDoKeyword_3_2_0() { return cDoKeyword_3_2_0; }
		
		//do_=Action
		public Assignment getDo_Assignment_3_2_1() { return cDo_Assignment_3_2_1; }
		
		//Action
		public RuleCall getDo_ActionParserRuleCall_3_2_1_0() { return cDo_ActionParserRuleCall_3_2_1_0; }
		
		//('exit:' exit_=Action)?
		public Group getGroup_3_3() { return cGroup_3_3; }
		
		//'exit:'
		public Keyword getExitKeyword_3_3_0() { return cExitKeyword_3_3_0; }
		
		//exit_=Action
		public Assignment getExit_Assignment_3_3_1() { return cExit_Assignment_3_3_1; }
		
		//Action
		public RuleCall getExit_ActionParserRuleCall_3_3_1_0() { return cExit_ActionParserRuleCall_3_3_1_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_3_4() { return cRightCurlyBracketKeyword_3_4; }
	}
	public class FinalStateElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.FinalState");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cFinalStateAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cEndKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameFINALSTATENAMETerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		
		//FinalState returns FinalState:
		//    {FinalState}
		//    'end' name=FINALSTATENAME;
		@Override public ParserRule getRule() { return rule; }
		
		//{FinalState}
		//'end' name=FINALSTATENAME
		public Group getGroup() { return cGroup; }
		
		//{FinalState}
		public Action getFinalStateAction_0() { return cFinalStateAction_0; }
		
		//'end'
		public Keyword getEndKeyword_1() { return cEndKeyword_1; }
		
		//name=FINALSTATENAME
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//FINALSTATENAME
		public RuleCall getNameFINALSTATENAMETerminalRuleCall_2_0() { return cNameFINALSTATENAMETerminalRuleCall_2_0; }
	}
	public class InitialStateElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.InitialState");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cInitialStateAction_0 = (Action)cGroup.eContents().get(0);
		private final Keyword cStartKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cNameAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cNameINITIALSTATENAMETerminalRuleCall_2_0 = (RuleCall)cNameAssignment_2.eContents().get(0);
		
		//InitialState returns InitialState:
		//    {InitialState}
		//    'start' name=INITIALSTATENAME;
		@Override public ParserRule getRule() { return rule; }
		
		//{InitialState}
		//'start' name=INITIALSTATENAME
		public Group getGroup() { return cGroup; }
		
		//{InitialState}
		public Action getInitialStateAction_0() { return cInitialStateAction_0; }
		
		//'start'
		public Keyword getStartKeyword_1() { return cStartKeyword_1; }
		
		//name=INITIALSTATENAME
		public Assignment getNameAssignment_2() { return cNameAssignment_2; }
		
		//INITIALSTATENAME
		public RuleCall getNameINITIALSTATENAMETerminalRuleCall_2_0() { return cNameINITIALSTATENAMETerminalRuleCall_2_0; }
	}
	public class TransitionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.Transition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cTransitionAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cFromAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final CrossReference cFromStateCrossReference_1_0 = (CrossReference)cFromAssignment_1.eContents().get(0);
		private final RuleCall cFromStateEStringParserRuleCall_1_0_1 = (RuleCall)cFromStateCrossReference_1_0.eContents().get(1);
		private final Keyword cHyphenMinusKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cTriggerAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cTriggerTriggerParserRuleCall_3_0 = (RuleCall)cTriggerAssignment_3.eContents().get(0);
		private final Group cGroup_4 = (Group)cGroup.eContents().get(4);
		private final Keyword cLeftSquareBracketKeyword_4_0 = (Keyword)cGroup_4.eContents().get(0);
		private final Assignment cGuardAssignment_4_1 = (Assignment)cGroup_4.eContents().get(1);
		private final RuleCall cGuardGuardParserRuleCall_4_1_0 = (RuleCall)cGuardAssignment_4_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_4_2 = (Keyword)cGroup_4.eContents().get(2);
		private final Group cGroup_5 = (Group)cGroup.eContents().get(5);
		private final Keyword cSolidusKeyword_5_0 = (Keyword)cGroup_5.eContents().get(0);
		private final Assignment cActionAssignment_5_1 = (Assignment)cGroup_5.eContents().get(1);
		private final RuleCall cActionActionParserRuleCall_5_1_0 = (RuleCall)cActionAssignment_5_1.eContents().get(0);
		private final Keyword cHyphenMinusGreaterThanSignKeyword_6 = (Keyword)cGroup.eContents().get(6);
		private final Assignment cToAssignment_7 = (Assignment)cGroup.eContents().get(7);
		private final CrossReference cToStateCrossReference_7_0 = (CrossReference)cToAssignment_7.eContents().get(0);
		private final RuleCall cToStateEStringParserRuleCall_7_0_1 = (RuleCall)cToStateCrossReference_7_0.eContents().get(1);
		
		///*
		// * You can define further more detailed validations rules to check for the semantics of your language
		// * in the '***Validator' class under 'src/***.validation'.
		// * e.g. There must be no Transitions to the InitialState and no Transitions from a FinalState
		//*/
		//Transition returns Transition:
		//    {Transition}
		//    from=[State|EString] '-'(trigger=Trigger)?('[' guard=Guard ']')? ('/' action=Action)? '->' to=[State|EString];
		@Override public ParserRule getRule() { return rule; }
		
		//{Transition}
		//from=[State|EString] '-'(trigger=Trigger)?('[' guard=Guard ']')? ('/' action=Action)? '->' to=[State|EString]
		public Group getGroup() { return cGroup; }
		
		//{Transition}
		public Action getTransitionAction_0() { return cTransitionAction_0; }
		
		//from=[State|EString]
		public Assignment getFromAssignment_1() { return cFromAssignment_1; }
		
		//[State|EString]
		public CrossReference getFromStateCrossReference_1_0() { return cFromStateCrossReference_1_0; }
		
		//EString
		public RuleCall getFromStateEStringParserRuleCall_1_0_1() { return cFromStateEStringParserRuleCall_1_0_1; }
		
		//'-'
		public Keyword getHyphenMinusKeyword_2() { return cHyphenMinusKeyword_2; }
		
		//(trigger=Trigger)?
		public Assignment getTriggerAssignment_3() { return cTriggerAssignment_3; }
		
		//Trigger
		public RuleCall getTriggerTriggerParserRuleCall_3_0() { return cTriggerTriggerParserRuleCall_3_0; }
		
		//('[' guard=Guard ']')?
		public Group getGroup_4() { return cGroup_4; }
		
		//'['
		public Keyword getLeftSquareBracketKeyword_4_0() { return cLeftSquareBracketKeyword_4_0; }
		
		//guard=Guard
		public Assignment getGuardAssignment_4_1() { return cGuardAssignment_4_1; }
		
		//Guard
		public RuleCall getGuardGuardParserRuleCall_4_1_0() { return cGuardGuardParserRuleCall_4_1_0; }
		
		//']'
		public Keyword getRightSquareBracketKeyword_4_2() { return cRightSquareBracketKeyword_4_2; }
		
		//('/' action=Action)?
		public Group getGroup_5() { return cGroup_5; }
		
		//'/'
		public Keyword getSolidusKeyword_5_0() { return cSolidusKeyword_5_0; }
		
		//action=Action
		public Assignment getActionAssignment_5_1() { return cActionAssignment_5_1; }
		
		//Action
		public RuleCall getActionActionParserRuleCall_5_1_0() { return cActionActionParserRuleCall_5_1_0; }
		
		//'->'
		public Keyword getHyphenMinusGreaterThanSignKeyword_6() { return cHyphenMinusGreaterThanSignKeyword_6; }
		
		//to=[State|EString]
		public Assignment getToAssignment_7() { return cToAssignment_7; }
		
		//[State|EString]
		public CrossReference getToStateCrossReference_7_0() { return cToStateCrossReference_7_0; }
		
		//EString
		public RuleCall getToStateEStringParserRuleCall_7_0_1() { return cToStateEStringParserRuleCall_7_0_1; }
	}
	public class ActionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.Action");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cActionAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cContentAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cContentActivityContentParserRuleCall_1_0 = (RuleCall)cContentAssignment_1.eContents().get(0);
		
		//Action returns Action:
		//    {Action}
		//    content=ActivityContent;
		@Override public ParserRule getRule() { return rule; }
		
		//{Action}
		//content=ActivityContent
		public Group getGroup() { return cGroup; }
		
		//{Action}
		public Action getActionAction_0() { return cActionAction_0; }
		
		//content=ActivityContent
		public Assignment getContentAssignment_1() { return cContentAssignment_1; }
		
		//ActivityContent
		public RuleCall getContentActivityContentParserRuleCall_1_0() { return cContentActivityContentParserRuleCall_1_0; }
	}
	public class TriggerElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.Trigger");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cTriggerAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cContentAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cContentActivityContentParserRuleCall_1_0 = (RuleCall)cContentAssignment_1.eContents().get(0);
		
		//Trigger returns Trigger:
		//    {Trigger}
		//    content=ActivityContent;
		@Override public ParserRule getRule() { return rule; }
		
		//{Trigger}
		//content=ActivityContent
		public Group getGroup() { return cGroup; }
		
		//{Trigger}
		public Action getTriggerAction_0() { return cTriggerAction_0; }
		
		//content=ActivityContent
		public Assignment getContentAssignment_1() { return cContentAssignment_1; }
		
		//ActivityContent
		public RuleCall getContentActivityContentParserRuleCall_1_0() { return cContentActivityContentParserRuleCall_1_0; }
	}
	public class GuardElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.Guard");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cGuardAction_0 = (Action)cGroup.eContents().get(0);
		private final Assignment cContentAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cContentEStringParserRuleCall_1_0 = (RuleCall)cContentAssignment_1.eContents().get(0);
		
		//Guard returns Guard:
		//    {Guard}
		//    content=EString;
		@Override public ParserRule getRule() { return rule; }
		
		//{Guard}
		//content=EString
		public Group getGroup() { return cGroup; }
		
		//{Guard}
		public Action getGuardAction_0() { return cGuardAction_0; }
		
		//content=EString
		public Assignment getContentAssignment_1() { return cContentAssignment_1; }
		
		//EString
		public RuleCall getContentEStringParserRuleCall_1_0() { return cContentEStringParserRuleCall_1_0; }
	}
	public class ActivityContentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.ActivityContent");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cSTRINGTerminalRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cCALLTerminalRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		
		///*
		// * You can use Strings syntax ('...') for unclear/abstract parts of your text syntax.
		// * If your domain is defined enough you can create rules and terminal rules for these parts,too.
		//*/
		//ActivityContent returns ecore::EString:
		//    STRING | ID | CALL;
		@Override public ParserRule getRule() { return rule; }
		
		//STRING | ID | CALL
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//STRING
		public RuleCall getSTRINGTerminalRuleCall_0() { return cSTRINGTerminalRuleCall_0; }
		
		//ID
		public RuleCall getIDTerminalRuleCall_1() { return cIDTerminalRuleCall_1; }
		
		//CALL
		public RuleCall getCALLTerminalRuleCall_2() { return cCALLTerminalRuleCall_2; }
	}
	public class EStringElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.EString");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cSTRINGTerminalRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cINITIALSTATENAMETerminalRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cFINALSTATENAMETerminalRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		
		//EString returns ecore::EString:
		//    STRING | ID | INITIALSTATENAME | FINALSTATENAME;
		@Override public ParserRule getRule() { return rule; }
		
		//STRING | ID | INITIALSTATENAME | FINALSTATENAME
		public Alternatives getAlternatives() { return cAlternatives; }
		
		//STRING
		public RuleCall getSTRINGTerminalRuleCall_0() { return cSTRINGTerminalRuleCall_0; }
		
		//ID
		public RuleCall getIDTerminalRuleCall_1() { return cIDTerminalRuleCall_1; }
		
		//INITIALSTATENAME
		public RuleCall getINITIALSTATENAMETerminalRuleCall_2() { return cINITIALSTATENAMETerminalRuleCall_2; }
		
		//FINALSTATENAME
		public RuleCall getFINALSTATENAMETerminalRuleCall_3() { return cFINALSTATENAMETerminalRuleCall_3; }
	}
	
	
	private final StateMachineElements pStateMachine;
	private final StateElements pState;
	private final NormalStateElements pNormalState;
	private final FinalStateElements pFinalState;
	private final InitialStateElements pInitialState;
	private final TransitionElements pTransition;
	private final ActionElements pAction;
	private final TriggerElements pTrigger;
	private final GuardElements pGuard;
	private final TerminalRule tINITIALSTATENAME;
	private final TerminalRule tFINALSTATENAME;
	private final ActivityContentElements pActivityContent;
	private final TerminalRule tCALL;
	private final EStringElements pEString;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public ParserGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pStateMachine = new StateMachineElements();
		this.pState = new StateElements();
		this.pNormalState = new NormalStateElements();
		this.pFinalState = new FinalStateElements();
		this.pInitialState = new InitialStateElements();
		this.pTransition = new TransitionElements();
		this.pAction = new ActionElements();
		this.pTrigger = new TriggerElements();
		this.pGuard = new GuardElements();
		this.tINITIALSTATENAME = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.INITIALSTATENAME");
		this.tFINALSTATENAME = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.FINALSTATENAME");
		this.pActivityContent = new ActivityContentElements();
		this.tCALL = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "swt.most.statemachine.xtext.Parser.CALL");
		this.pEString = new EStringElements();
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("swt.most.statemachine.xtext.Parser".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//StateMachine returns StateMachine:
	//    {StateMachine}
	//    'StateMachine' name=EString
	//    '{'
	//        initialstate=InitialState
	//        ( states+=NormalState | finalstates+=FinalState | transitions+=Transition)*
	//    '}';
	public StateMachineElements getStateMachineAccess() {
		return pStateMachine;
	}
	
	public ParserRule getStateMachineRule() {
		return getStateMachineAccess().getRule();
	}
	
	//State returns State:
	//    InitialState | FinalState | NormalState;
	public StateElements getStateAccess() {
		return pState;
	}
	
	public ParserRule getStateRule() {
		return getStateAccess().getRule();
	}
	
	//NormalState returns NormalState:
	//    {NormalState}
	//    'state' name=EString ('{'
	//        ('entry:' entry_=Action)?
	//        ('do:' do_=Action)?
	//        ('exit:' exit_=Action)?
	//    '}')?;
	public NormalStateElements getNormalStateAccess() {
		return pNormalState;
	}
	
	public ParserRule getNormalStateRule() {
		return getNormalStateAccess().getRule();
	}
	
	//FinalState returns FinalState:
	//    {FinalState}
	//    'end' name=FINALSTATENAME;
	public FinalStateElements getFinalStateAccess() {
		return pFinalState;
	}
	
	public ParserRule getFinalStateRule() {
		return getFinalStateAccess().getRule();
	}
	
	//InitialState returns InitialState:
	//    {InitialState}
	//    'start' name=INITIALSTATENAME;
	public InitialStateElements getInitialStateAccess() {
		return pInitialState;
	}
	
	public ParserRule getInitialStateRule() {
		return getInitialStateAccess().getRule();
	}
	
	///*
	// * You can define further more detailed validations rules to check for the semantics of your language
	// * in the '***Validator' class under 'src/***.validation'.
	// * e.g. There must be no Transitions to the InitialState and no Transitions from a FinalState
	//*/
	//Transition returns Transition:
	//    {Transition}
	//    from=[State|EString] '-'(trigger=Trigger)?('[' guard=Guard ']')? ('/' action=Action)? '->' to=[State|EString];
	public TransitionElements getTransitionAccess() {
		return pTransition;
	}
	
	public ParserRule getTransitionRule() {
		return getTransitionAccess().getRule();
	}
	
	//Action returns Action:
	//    {Action}
	//    content=ActivityContent;
	public ActionElements getActionAccess() {
		return pAction;
	}
	
	public ParserRule getActionRule() {
		return getActionAccess().getRule();
	}
	
	//Trigger returns Trigger:
	//    {Trigger}
	//    content=ActivityContent;
	public TriggerElements getTriggerAccess() {
		return pTrigger;
	}
	
	public ParserRule getTriggerRule() {
		return getTriggerAccess().getRule();
	}
	
	//Guard returns Guard:
	//    {Guard}
	//    content=EString;
	public GuardElements getGuardAccess() {
		return pGuard;
	}
	
	public ParserRule getGuardRule() {
		return getGuardAccess().getRule();
	}
	
	///*
	// * Special naming conventions
	// * Also possible via validator rules
	//*/
	//terminal INITIALSTATENAME returns ecore::EString:
	//    ('I_')(INT|STRING|ID)+;
	public TerminalRule getINITIALSTATENAMERule() {
		return tINITIALSTATENAME;
	}
	
	//terminal FINALSTATENAME returns ecore::EString:
	//    ('F_')(INT|STRING|ID)+ ;
	public TerminalRule getFINALSTATENAMERule() {
		return tFINALSTATENAME;
	}
	
	///*
	// * You can use Strings syntax ('...') for unclear/abstract parts of your text syntax.
	// * If your domain is defined enough you can create rules and terminal rules for these parts,too.
	//*/
	//ActivityContent returns ecore::EString:
	//    STRING | ID | CALL;
	public ActivityContentElements getActivityContentAccess() {
		return pActivityContent;
	}
	
	public ParserRule getActivityContentRule() {
		return getActivityContentAccess().getRule();
	}
	
	//terminal CALL returns ecore::EString:
	//    (ID)('.'(ID))+;
	public TerminalRule getCALLRule() {
		return tCALL;
	}
	
	//EString returns ecore::EString:
	//    STRING | ID | INITIALSTATENAME | FINALSTATENAME;
	public EStringElements getEStringAccess() {
		return pEString;
	}
	
	public ParserRule getEStringRule() {
		return getEStringAccess().getRule();
	}
	
	//terminal ID: '^'?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt: ('0'..'9')+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//            '"' ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|'"') )* '"' |
	//            "'" ( '\\' . /* 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' */ | !('\\'|"'") )* "'"
	//        ;
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT : '/*' -> '*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT : '//' !('\n'|'\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS         : (' '|'\t'|'\r'|'\n')+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER: .;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
