/*
 * generated by Xtext 2.35.0
 */
package swt.most.statemachine.xtext;


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class ParserRuntimeModule extends AbstractParserRuntimeModule {
}
