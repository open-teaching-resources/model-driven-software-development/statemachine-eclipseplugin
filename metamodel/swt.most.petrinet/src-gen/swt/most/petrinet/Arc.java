/**
 */
package swt.most.petrinet;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.petrinet.PetrinetPackage#getArc()
 * @model abstract="true"
 * @generated
 */
public interface Arc extends EObject {
} // Arc
