/**
 */
package swt.most.petrinet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Incoming Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link swt.most.petrinet.IncomingArc#getTarget <em>Target</em>}</li>
 *   <li>{@link swt.most.petrinet.IncomingArc#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see swt.most.petrinet.PetrinetPackage#getIncomingArc()
 * @model
 * @generated
 */
public interface IncomingArc extends Arc {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Transition)
	 * @see swt.most.petrinet.PetrinetPackage#getIncomingArc_Target()
	 * @model required="true"
	 * @generated
	 */
	Transition getTarget();

	/**
	 * Sets the value of the '{@link swt.most.petrinet.IncomingArc#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Transition value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Place)
	 * @see swt.most.petrinet.PetrinetPackage#getIncomingArc_Source()
	 * @model required="true"
	 * @generated
	 */
	Place getSource();

	/**
	 * Sets the value of the '{@link swt.most.petrinet.IncomingArc#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Place value);

} // IncomingArc
