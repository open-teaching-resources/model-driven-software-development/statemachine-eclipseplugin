/**
 */
package swt.most.petrinet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Outgoing Arc</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link swt.most.petrinet.OutgoingArc#getSource <em>Source</em>}</li>
 *   <li>{@link swt.most.petrinet.OutgoingArc#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see swt.most.petrinet.PetrinetPackage#getOutgoingArc()
 * @model
 * @generated
 */
public interface OutgoingArc extends Arc {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Transition)
	 * @see swt.most.petrinet.PetrinetPackage#getOutgoingArc_Source()
	 * @model required="true"
	 * @generated
	 */
	Transition getSource();

	/**
	 * Sets the value of the '{@link swt.most.petrinet.OutgoingArc#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Transition value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Place)
	 * @see swt.most.petrinet.PetrinetPackage#getOutgoingArc_Target()
	 * @model required="true"
	 * @generated
	 */
	Place getTarget();

	/**
	 * Sets the value of the '{@link swt.most.petrinet.OutgoingArc#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Place value);

} // OutgoingArc
