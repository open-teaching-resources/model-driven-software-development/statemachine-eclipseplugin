/**
 */
package swt.most.petrinet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Place</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link swt.most.petrinet.Place#getToken <em>Token</em>}</li>
 * </ul>
 *
 * @see swt.most.petrinet.PetrinetPackage#getPlace()
 * @model
 * @generated
 */
public interface Place extends Element {
	/**
	 * Returns the value of the '<em><b>Token</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token</em>' containment reference.
	 * @see #setToken(Token)
	 * @see swt.most.petrinet.PetrinetPackage#getPlace_Token()
	 * @model containment="true"
	 * @generated
	 */
	Token getToken();

	/**
	 * Sets the value of the '{@link swt.most.petrinet.Place#getToken <em>Token</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token</em>' containment reference.
	 * @see #getToken()
	 * @generated
	 */
	void setToken(Token value);

} // Place
