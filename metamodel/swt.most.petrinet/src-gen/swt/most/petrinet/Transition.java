/**
 */
package swt.most.petrinet;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.petrinet.PetrinetPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends Element {
} // Transition
