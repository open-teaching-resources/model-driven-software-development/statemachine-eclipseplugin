/**
 */
package swt.most.statemachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.statemachine.StatemachinePackage#getAction()
 * @model
 * @generated
 */
public interface Action extends Expression {
} // Action
