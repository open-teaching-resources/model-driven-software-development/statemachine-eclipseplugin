/**
 */
package swt.most.statemachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.statemachine.StatemachinePackage#getFinalState()
 * @model
 * @generated
 */
public interface FinalState extends State {
} // FinalState
