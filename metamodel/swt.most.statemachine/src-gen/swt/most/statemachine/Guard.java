/**
 */
package swt.most.statemachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.statemachine.StatemachinePackage#getGuard()
 * @model
 * @generated
 */
public interface Guard extends Expression {
} // Guard
