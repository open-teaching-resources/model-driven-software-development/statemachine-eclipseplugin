/**
 */
package swt.most.statemachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.statemachine.StatemachinePackage#getInitialState()
 * @model
 * @generated
 */
public interface InitialState extends State {
} // InitialState
