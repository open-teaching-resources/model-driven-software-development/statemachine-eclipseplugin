/**
 */
package swt.most.statemachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see swt.most.statemachine.StatemachinePackage#getTrigger()
 * @model
 * @generated
 */
public interface Trigger extends Expression {
} // Trigger
