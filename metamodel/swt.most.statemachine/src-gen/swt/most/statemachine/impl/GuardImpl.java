/**
 */
package swt.most.statemachine.impl;

import org.eclipse.emf.ecore.EClass;

import swt.most.statemachine.Guard;
import swt.most.statemachine.StatemachinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Guard</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class GuardImpl extends ExpressionImpl implements Guard {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GuardImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.GUARD;
	}

} //GuardImpl
