/**
 */
package swt.most.statemachine.impl;

import org.eclipse.emf.ecore.EClass;

import swt.most.statemachine.StatemachinePackage;
import swt.most.statemachine.Trigger;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TriggerImpl extends ExpressionImpl implements Trigger {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.TRIGGER;
	}

} //TriggerImpl
