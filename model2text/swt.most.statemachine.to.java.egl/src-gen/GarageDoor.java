public class GarageDoor{

    // Some parts of the implementation are simplified, 
	// because implementing them would require more information then the state machine model provides.
	
    // Attributes of the state machine
	// Current active state
	private State currentState = new GarageDoor();
	
	// Constructor of the state machine
	// Also triggers the default (not named) Transition
	public GarageDoor(){
		this._default();
	}

	// Triggers used in the state machine
	// Default trigger for transitions without trigger
	// This method is always automatically activated as soon as the state is entered or a time interval has passed
	public void _default(){ currentState._default(); } 
	// Triggers that were extracted from the state machine transitions


	// States as inner classes
	// Abstract state for  the usage of polymorphism in the pattern
	private abstract class State{
		public void _default(){ /* by default, do nothing */ }
	}

	// Initial state
	private class Initial{
		// Initial states should only have one default transition without guards and actions
		public void _default(){
				currentState = new Closed();
				currentState._default();
			}
	}

	// Normal states
	private class Closed extends State{
		public void openDoor(){
			if(true){ // No guard
				motor.upwards(); // Action
				currentState = new Opening();
				currentState._default();
			}	
		}
					
		public void _default(){
			if(shutdown){ // Special guard, notation simplified in this example
				currentState = new Final();
				currentState._default();
			}	
		}

		public void closeDoor(){
			if(true){ // No guard
				currentState = new Closed();
				currentState._default();
			}	
		}
					
	}
	
	private class Open extends State{
		public void closeDoor(){
			if(true){ // No guard
				motor.downwards(); // Action
				currentState = new Closing();
				currentState._default();
			}	
		}
					
		public void openDoor(){
			if(true){ // No guard
				currentState = new Open();
				currentState._default();
			}	
		}
					
	}
	
	private class Opening extends State{
		public void _default(){
			if(stopper){ // Special guard, notation simplified in this example
				motor.stop(); // Action
				currentState = new Open();
				currentState._default();
			}	
		}

		public void closeDoor(){
			if(true){ // No guard
				motor.downwards(); // Action
				currentState = new Closing();
				currentState._default();
			}	
		}
					
		public void openDoor(){
			if(true){ // No guard
				currentState = new Opening();
				currentState._default();
			}	
		}
					
	}
	
	private class Closing extends State{
		public void _default(){
			if(stopper){ // Special guard, notation simplified in this example
				motor.stop(); // Action
				currentState = new Closed();
				currentState._default();
			}	
		}

		public void openDoor(){
			if(true){ // No guard
				motor.upwards(); // Action
				currentState = new Opening();
				currentState._default();
			}	
		}
					
		public void closeDoor(){
			if(true){ // No guard
				currentState = new Closing();
				currentState._default();
			}	
		}
					
	}
	
 
	
	// Final states
	private class Final extends State{
		// A final state is only a flow sink without functionality
	}

 
}