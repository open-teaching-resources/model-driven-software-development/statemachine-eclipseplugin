public class Switch{

    // Some parts of the implementation are simplified, 
	// because implementing them would require more information then the state machine model provides.
	
    // Attributes of the state machine
	// Current active state
	private State currentState = new Switch();
	
	// Constructor of the state machine
	// Also triggers the default (not named) Transition
	public Switch(){
		this._default();
	}

	// Triggers used in the state machine
	// Default trigger for transitions without trigger
	// This method is always automatically activated as soon as the state is entered or a time interval has passed
	public void _default(){ currentState._default(); } 
	// Triggers that were extracted from the state machine transitions


	// States as inner classes
	// Abstract state for  the usage of polymorphism in the pattern
	private abstract class State{
		public void _default(){ /* by default, do nothing */ }
	}

	// Initial state
	private class Initial{
		// Initial states should only have one default transition without guards and actions
		public void _default(){
				currentState = new Off();
				currentState._default();
			}
	}

	// Normal states
	private class On extends State{
		public void flip(){
			if(true){ // No guard
				currentState = new Off();
				currentState._default();
			}	
		}
					
	}
	
	private class Off extends State{
		public void flip(){
			if(true){ // No guard
				currentState = new On();
				currentState._default();
			}	
		}
					
	}
	
 
	
	// Final states
 
}